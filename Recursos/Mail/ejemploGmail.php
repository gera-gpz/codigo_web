<?php

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Etc/UTC');

require '../../../Libraries/PHPMailer/PHPMailerAutoload.php';

$mail = new PHPMailer;			  //Create a new PHPMailer instance
$mail->isSMTP();				  //Tell PHPMailer to use SMTP
$mail->SMTPDebug = 2;             //Enable SMTP debugging 2 = client and server messages
$mail->Debugoutput = 'html';      //Ask for HTML-friendly debug output
$mail->Host = 'smtp.gmail.com';   //Set the hostname of the mail server
$mail->Port = 587; 				  //Set the SMTP port number - 587 for authenticated TLS
$mail->SMTPSecure = 'tls'; 		  //Set the encryption system to use - ssl (deprecated) or tls


$mail->SMTPAuth = true;						 //Whether to use SMTP authentication
$mail->Username = "gerardop@infinitogl.com"; //Username for SMTP authentication (full email address for gmail)
$mail->Password = "gerardo2225!";			 //Password to use for SMTP authentication


$mail->setFrom('gerardop@infinitogl.com', 'First Last');       	//Set who the message is to be sent from
$mail->addReplyTo('gerardop@infinitogl.com', 'First Last');		//Set an alternative reply-to address
$mail->addAddress('gera_gpz@hotmail.com', 'Gerardo GPZ');		//Set who the message is to be sent to
$mail->Subject = 'Ejemplo de Envio de Mail desde PHP';			//Set the subject line

//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$mail->msgHTML(file_get_contents('contenidoMail.html'), dirname(__FILE__));
$mail->AltBody = 'This is a plain-text message body';	//Replace the plain text body with one created manually
$mail->addAttachment('phpmailer_mini.png');				//Attach an image file 

//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    echo "Message sent!";
}
