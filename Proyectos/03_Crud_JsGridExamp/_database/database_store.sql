create database store;
use store;

create table countries(
    id       int not null auto_increment,
    name     varchar(250) not null,
    primary  key(id)
);

create table clients(
    id          int not null auto_increment,
    name        varchar(250) not null,
    age         int not null,
    address     varchar(250) not null,
    married     bit not null,
    country_id  int,
    primary key (id),
    foreign key (country_id) references countries(id) on delete set null
);

insert into countries(name) 
values ("Mexico"),("EstadosUnidos"),("Canada");


insert into clients(name,age,address,married,country_id) 
values ("Gerardo",45,"Ocampo 1",0,1),
       ("Fabian",46,"Guerrero 2",0,1),
       ("Ramiro",47,"Guerrero 3",0,1);

select * from clients;
select * from countries;
