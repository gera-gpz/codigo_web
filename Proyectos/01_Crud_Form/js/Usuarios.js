var tipoOperacion=0;

$('document').ready(function () {

    // $("#buscar").autocomplete({
        // source: "./php/UsuarioBuscarObj.php",
        // minLength: 1,
        // focus: function (event, ui) { event.preventDefault();},
        // select: function ( event, ui ) {
            // event.preventDefault();
            // $("#nombre").val(ui.item.label);
            // $("#idUsuario").val(ui.item.value.idUsuario);
            // $("#apPaterno").val(ui.item.value.apPaterno);
            // $("#apMaterno").val(ui.item.value.apMaterno);
            // $("#login").val(ui.item.value.login);
            // $("#password").val(ui.item.value.password);
            // $("#buscar").val(ui.item.value.idUsuario);
            // $('#b_nuevo').prop("disabled", false);     //Enabled
            // $('#b_grabar').prop("disabled", true);     //Disabled
            // $('#b_eliminar').prop("disabled", false);  //Enabled
        // }
    // });


    $("#b_consultar").click(function () {
        $('input').val('');
        var vidu = prompt("ID de Usuario:");


        $.post('./php/UsuarioConsultar.php',
                {idUsuario: vidu},
                function (ret) {

                if (ret['resultado'] != 0) {

                console.log("Error");

                $('#modalMensaje .modal-header').addClass('modal-header-danger');
                $('#modalMensaje .modal-header h2').text(ret['mensaje']);
                $('#modalMensaje .modal-body h3').text(ret['detalle']);
                $('#modalMensaje').modal();

                $("#modalMensaje").on('shown.bs.modal', function () {
                    $('#botonCerrar').focus();
                });
                $("#modalMensaje").on('hidden.bs.modal', function () {
                    $('#modalMensaje .modal-header').removeClass('modal-header-danger');
                });
                }
                else {

                console.log(ret);


                $('#idUsuario').val(ret.detalle.idUsuario);
                $('#nombre').val(ret.detalle.nombre);
                $('#apPaterno').val(ret.detalle.apPaterno);
                $('#apMaterno').val(ret.detalle.apPaterno);
                $('#login').val(ret.detalle.login);
                $('#password').val(ret.detalle.password);


                $('#modalMensaje .modal-header').addClass('modal-header-success');
                $('#modalMensaje .modal-header h2').text(ret['mensaje']);
                $('#modalMensaje .modal-body h3').text("Nombre: "+ret.detalle.nombre+' '+ret.detalle.apPaterno);
                $('#modalMensaje').modal();

                $("#modalMensaje").on('shown.bs.modal', function () {
                    $('#botonCerrar').focus();
                });
                $("#modalMensaje").on('hidden.bs.modal', function () {
                    $('#myModal .modal-header').removeClass('modal-header-success');
                });

                $('#b_nuevo').prop("disabled", false);    //Enabled
                $('#b_grabar').prop("disabled", true);    //Disabled
                $('#b_eliminar').prop("disabled", false); //Enabled
                $('#b_modificar').prop("disabled", false); //Enabled
            }
        },'json');

    });


    $("#b_limpiar").click(function () {
        $('input').val('');
        $('.form-control').prop("disabled", true);
        $('#b_nuevo').prop("disabled", false);
        $('#b_grabar').prop("disabled", true);
        $('#b_eliminar').prop("disabled", true);
        $('#b_consultar').prop("disabled", false);
        $('#b_modificar').prop("disabled", true);
    });



    $("#b_nuevo").click(function () {
        tipoOperacion = 1;
        $('input').val('');
        $('#b_nuevo').prop("disabled", true);
        $('#b_grabar').prop("disabled", false);
        $('#b_eliminar').prop("disabled", true);
        $('#b_consultar').prop("disabled", true);

        $('.form-control').prop("disabled", false);
        $('#idUsuario').prop("disabled", true);
        $("#nombre").focus();
    });

    $("#b_modificar").click(function () {
        tipoOperacion = 2;
        $('#b_nuevo').prop("disabled", true);
        $('#b_grabar').prop("disabled", false);
        $('#b_eliminar').prop("disabled", true);
        $('#b_modificar').prop("disabled", true);
        $('#b_consultar').prop("disabled", true);

        $('.form-control').prop("disabled", false);
        $('#idUsuario').prop("disabled", true);
        $("#nombre").focus();
    });


    $("#b_grabar").click(function () {
        var vidu = $('#idUsuario').val();
        var vnom = $('#nombre').val();
        var vapp = $('#apPaterno').val();
        var vapm = $('#apMaterno').val();
        var vlog = $('#login').val();
        var vpas = $('#password').val();

        if (vnom=="" || vapp=="" || vlog=="" || vpas=="") {
            console.log("Falto capturar informacion");
            $('#modalMensaje .modal-header').addClass('modal-header-danger');
            $('#modalMensaje .modal-header h2').text("Formulario Incompleto");
            $('#modalMensaje .modal-body h3').text("Debe capturar todos los campos");
            $('#modalMensaje').modal();
            $("#modalMensaje").on('shown.bs.modal', function () {
                $('#botonCerrar').focus();
            });
            $("#modalMensaje").on('hidden.bs.modal', function () {
                $('#modalMensaje .modal-header').removeClass('modal-header-danger');
                $("#nombre").focus();
            });


        } else {
        $.post('./php/UsuarioGrabar.php',
                {idu: vidu, nom: vnom, app: vapp, apm: vapm, log: vlog, pas: vpas, tip:tipoOperacion},
                function (ret) {

                if (ret.resultado!= 0) {

                console.log('Error Insercion');

                $('#modalMensaje .modal-header').addClass('modal-header-danger');
                $('#modalMensaje .modal-header h2').text(ret['mensaje']);
                $('#modalMensaje .modal-body h3').text(ret['detalle']);
                $('#modalMensaje').modal();

                $("#modalMensaje").on('shown.bs.modal', function () {
                    $('#botonCerrar').focus();
                });
                $("#modalMensaje").on('hidden.bs.modal', function () {
                    $('#modalMensaje .modal-header').removeClass('modal-header-danger');
                });
                }
                else {
                $('#idUsuario').val(ret.detalle);

                $('#modalMensaje .modal-header').addClass('modal-header-success');
                $('#modalMensaje .modal-header h2').text(ret['mensaje']);
                $('#modalMensaje .modal-body h3').text("Usuario : "+ret['detalle']);
                $('#modalMensaje').modal();

                $("#modalMensaje").on('shown.bs.modal', function () {
                    $('#botonCerrar').focus();
                });
                $("#modalMensaje").on('hidden.bs.modal', function () {
                    $('#myModal .modal-header').removeClass('modal-header-success');
                });

                $('#b_nuevo').prop("disabled", false);     //Enabled
                $('#b_grabar').prop("disabled", true);     //Disabled
                $('#b_eliminar').prop("disabled", false);  //Enabled
                $('#b_modificar').prop("disabled", false); //Enabled
                $('#b_consultar').prop("disabled", false); //Enabled
            }
        },'json');

        $('.form-control').prop("disabled", true);
        $('#b_nuevo').prop("disabled",true);
        $('#b_grabar').prop("disabled",false);
        $('#b_eliminar').prop("disabled",true);
        $('#b_modificar').prop("disabled",true);
        $("#nombre").focus();
        }
    });



    $('#b_eliminar').click( function() {
        var vidu = $('#idUsuario').val();

        if (confirm('Borrar')) {
            $.post('./php/UsuarioBorrar.php',
            {idUsuario: vidu},
            function (ret) {
                alert("Borrado");
            },'json');

            $('input').val('');
            $('.form-control').prop("disabled", true);
            $('#b_nuevo').prop("disabled", false);
            $('#b_grabar').prop("disabled", true);
            $('#b_eliminar').prop("disabled", true);
        } else {
            alert("No se borra");
        }
    });

});