
-- drop database ejemplo2;
create database ejemplo2;
use ejemplo2;

-- ----------------------------------------------
-- Tabla clientes
-- ----------------------------------------------
-- drop table clientes;
create table clientes
(	id_cliente  integer,
    nombre		varchar(50),
    direccion	varchar(50),
    telefono	varchar(15)
    ciudad   	varchar(25)
    estado	    varchar(25)
    pais	    varchar(25)
);

insert into clientes() values(1,"Gerardo Pineda","Jalisco 726","718 2757","Laredo","Texas","Estados Unidos");
insert into clientes() values(2,"Adolfo Perez","Campeche 1836","715 7816","Nuevo Laredo","Tamaulipas",'Mexico');
insert into clientes() values(3,"Sergio Zamora","Washington 913","714 4844","Monterrey","Nuevo Leon","Mexico");
insert into clientes() values(4,"Norma Cruz","Anacanes 2526","714 3313","Houstos","Texas","Estados Unidos");
insert into clientes() values(5,"Carlos Alvarez","Guerrero 2301","151 4740","Monterrey","Nuevo Leon","Mexico");


select * from clientes