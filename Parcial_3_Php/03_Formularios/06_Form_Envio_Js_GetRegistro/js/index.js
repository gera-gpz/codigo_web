$(document).ready(function() {

        $('#btnConsultaBD').click(function() {

      // let datos = {idEmp:"1"};
      let datos = new FormData();
      datos.set("idEmp",1);

      let opciones= {
            method:"POST",
            headers: {'Content-Type': 'application/json',},
            body: JSON.stringify(datos)
      }

      fetch('./php/getRegistroMysqli.php',opciones)
            .then(respuesta => respuesta.json())
                .then(function(dato) { refrescar(dato) });

      });

      function refrescar(objeto) {
            console.log(objeto);
            $('#idCliente').val(objeto.idcliente);
            $('#nombre').val(objeto.nombre);
            $('#hora').val(objeto.hora);
            $('#direccion').val(objeto.direccion);
            $('#telefono').val(objeto.telefono);
            $('#ciudad').val(objeto.ciudad);
            $('#estado').val(objeto.estado);
            $('#pais').val(objeto.pais);
      }

});
