<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Get</title>
    <style>
         table, th, td {
          border: 1px solid black;
          border-collapse: collapse;
        }
        th, td {
          background-color: #96D4D4;
        }
    </style>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Dirección</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?php echo $_GET['nombre']?></td>
                <td><?php echo $_GET['direccion']?></td>
            </tr>
        </tbody>
    </table>
    <h4>Su comentario fue:<h1>
            <p><?php echo $_GET['comentario']?></p>
</body>
</html>