<?php
//Librerias para Generacion de Excel
require_once('../..//Recursos/PHPExcel/Classes/PHPExcel.php');
require_once('../..//Recursos/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php');
require_once('../..//Recursos/PHPExcel/Classes/PHPExcel/Cell/AdvancedValueBinder.php');

// Set value binder
PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_AdvancedValueBinder() );


//Genera Xls
$libroExcel = new PHPExcel();

//Establece fuente
$libroExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
$libroExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);

// Set column widths
$libroExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$libroExcel->getActiveSheet()->getColumnDimension('B')->setWdth(14);

$libroExcel->getActiveSheet()
           ->setCellValue('A8', '1975-05-19');
$libroExcel->getActiveSheet()
           ->getStyle('A8')
           ->getNumberFormat()
           ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

// Rename worksheet
$libroExcel->getActiveSheet()->setTitle('Advanced value binder');

// Set active sheet index
$libroExcel->setActiveSheetIndex(0);

// Guarda xls
$objWriter = new PHPExcel_Writer_Excel2007($libroExcel);
$nombreReporteExcel = 'EjemploFormato.xlsx';
$objWriter->save(str_replace('.php', '.xlsx', $nombreReporteExcel));
?>