<?php
$servidor = "localhost";
$basedatos= "ejemplo";
$usuario  = "root";
$password = "";

$con       = mysqli_connect($servidor,$usuario,$password,$basedatos) or die("No se pudo conectar a servidor");
$consulta  = "select * from empleado";
$registros = mysqli_query($con,$consulta) or die("Problemas en el select");


// Iterar sobre el Resultado como array asociativo
// -----------------------------------------------
// $result = mysqli_fetch_array($registros,MYSQLI_ASSOC)) {
// printf($result['nombre'].' '.$result['apPaterno'].'<br>');
//}


// Iterar sobre el Resultado como array numerico
// ---------------------------------------------
// while ($result = mysqli_fetch_array($registros,MYSQLI_NUM)) {
//     printf($result[0].' '.$result[1].'<br>');
// }

// Obtiene Resultado de la consulta como una matriz
// -------------------------------------------------
$result = mysqli_fetch_all($registros,MYSQLI_ASSOC);


mysqli_close($con);
echo json_encode($result);
?>