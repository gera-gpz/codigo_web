$(document).ready(function() {         

    const opcionesAjax = {
          "async": true,
          "crossDomain": true,
          "url": "./Grid.php",
          "method": "GET",
          "headers": {"Accept": "*/*"}
    };
  
    $.ajax(opcionesAjax).done(function (respuesta) {
        let tabla = JSON.parse(respuesta);
      
        $("#jsGrid").jsGrid({
        width:    "100%",
        height:   "400px",
        inserting:true,
        editing:  true,
        sorting:  true,
        paging:   true,
        data:     tabla,
        fields: [{ name: "idEmpleado", type: "number", valueField: "ID", width: 10  },
                 { name: "nombre",     type: "text",   width: 150 },
                 { name: "apPaterno",  type: "text",   width: 150 },
                 { name: "apMaterno",  type: "text",   width: 150 },
                 { name: "edad",       type: "number", width: 150 },
                 { name: "sueldo",     type: "sueldo", width: 150}]
        });
    });
});