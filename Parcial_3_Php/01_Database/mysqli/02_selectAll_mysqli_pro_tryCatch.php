<?php
$servidor = "localhost";
$basedatos= "ejemplo";
$usuario  = "root";
$password = "";

mysqli_report(MYSQLI_REPORT_STRICT | MYSQLI_REPORT_ALL);

try {
    $con = mysqli_connect($servidor,$usuario,$password,$basedatos) or die("No se pudo conectar a localhost");

} catch (mysqli_sql_exception $e) {
    $resultado['estado']=0;
    $resultado['datos']="No se pudo conectar a la BD: ".mysqli_connect_error();
}

$estadoConexion= mysqli_connect_error(); //regresa null si la conexion esta ok

if (is_null($estadoConexion)) {

    $sentenciaSql= "selec * fro empleado";
    try {
        $result     = mysqli_query($con,$sentenciaSql);
        $resultSet  = mysqli_fetch_all($result,MYSQLI_ASSOC);
        echo $resultSet;
        
        $resultado['estado']= 1;
        $resultado['dato']  = $resultSet;

    } catch (mysqli_sql_exception $e) {
        $resultado['estado']= 0;
        $resultado['datos'] = "Error en la consulta: ".mysqli_error($con); 
    }
    mysqli_close($con); //Cierra conexion si esta abierta
}

echo json_encode($resultado);
?>