<?php

$servidor  = 'localhost';
$basedatos = 'ejemplo';
$usuario   = 'root';
$password  = '';

try {
    $dbh = new PDO("mysql:host=$servidor;dbname=$basedatos", $usuario, $password);
} catch(PDOException $e) {
    $row['resultado']  = '1';
    $row['detalle']    = $e->getMessage();
    echo json_encode($row);
    return;
}

$consultaSql = "select * from cusuario";
$sentencia = $dbh->prepare($consultaSql);

 if($sentencia->execute()){
       $resultSet = $sentencia->fetch(PDO::FETCH_ASSOC);

       $row['resultado']  = '0';
       $row['detalle']    = $resultSet;
} else {
       $row['resultado']  = '2';
       $row['detalle']    = "Error al ejectar la consulta";
}

echo json_encode($row);
?>

