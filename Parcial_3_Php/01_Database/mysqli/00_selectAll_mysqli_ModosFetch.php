<?php
$servidor   = "localhost";
$basedatos  = "ejemplo";
$usuario    = "root";
$password   = "";
$con        = mysqli_connect($servidor,$usuario,$password,$basedatos) or die("No se pudo conectar a localhost");
$consultaSql= "select * from empleado";
$resultado  = mysqli_query($con,$consultaSql) or die("Problemas en el select");

// Iterar sobre el resultado de la consulta como array asociativo
// while ($registro = mysqli_fetch_array($resultado,MYSQLI_ASSOC)) {
//     printf($registro['nombre'].' '.$registro['apPaterno'].'<br>');
// }

// Iterar sobre el resultado de la consulta como array numerico
// while ($registro = mysqli_fetch_array($resultado,MYSQLI_NUM)) {
//     printf($registro[0].' '.$registro[1].'<br>');
// }

// Obtiene todo el resultado de la consulta como una matriz
$resultSet = mysqli_fetch_all($resultado);
// var_dump($resultSet);

// Obtiene Resultado de la consulta como un array de objetos
// $resultSet = mysqli_fetch_all($resultado,MYSQLI_ASSOC);
//var_dump($resultSet);

mysqli_close($con);
echo json_encode($resultSet);
?>