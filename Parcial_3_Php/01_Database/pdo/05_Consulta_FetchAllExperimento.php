<?php
$hostname='localhost';
$database='crud'; 
$username='root'; 
$password=''; 

try {
      $con = new PDO("mysql:host=$hostname;dbname=$database",$username,$password);
} catch(PDOException $e) {
      echo "Error de conexion a la base de datos";
      echo $e->getMessage();
      exit();
}
$con->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

try {
    $consultaSql = "select nombre,apPaterno,apMaterno,login from cusuario";
    $consulta = $con -> prepare($consultaSql);
    $consulta -> execute();

    $resultado = $consulta->fetchAll(PDO::FETCH_ASSOC);
    var_dump($resultado);

    //Iterar sobde el resultado de la consulta mediante un for
    //--------------------------------------------------------
    // for ($i=0; $i<=count($resultado)-1; $i++) {
    //    print "<b>nombre    =</b> ".$resultado[$i]['nombre']."<br>";
    //    print "<b>apPaterno =</b> ".$resultado[$i]['apPaterno']."<br>";
    //    print "<b>apMaterno =</b> ".$resultado[$i]['apMaterno']."<br>";
    //    print "<b>Login     =</b> ".$resultado[$i]['login']."<br>";
    // }


    // Iterar sobre el resultado de la consulta con un for/foreach
    // Cambiando el nombre de los indices
    // -----------------------------------------------
    $nuevoResultado=array();
    for ($i=0; $i<=count($resultado)-1; $i++) {
        foreach( $resultado[$i] as $index => $value) {
                //Tomar primeros 3 caracteres del indice
                $nuevoIndice = substr($index,0,3); 
                $nuevoResultado[$i][$nuevoIndice]=$value;  
                }
        }
    
    var_dump($nuevoResultado);
    
    $consulta->closeCursor();
}
catch(PDOException $e) {
        echo "Error de consulta a la base de datos";
        echo $e->getMessage();
}
// echo json_encode($resultado);
?>