<?php
// Funcion json_decode()  
// Convierte:    Cadena Json   =>    Variable Php (array or object)


// VariablePhp String que contiene una cadena json
// -----------------------------------------------
$cadenaJson='{"numero":"10","nombre":"Gerardo","direccion":"Washington","telefono":"1000691",
              "lenguajes":["Sql","Html","Css","Javascript","Jquery","Php","Perl"]}';


//Se convierte la variablePhp string a objeto
//-------------------------------------------
$info = json_decode($cadenaJson);

echo 'numero    : '.$info->numero.'<br>';
echo 'nombre    : '.$info->nombre.'<br>';
echo 'direccion : '.$info->direccion.'<br>'; 
echo 'telefono  : '.$info->lenguajes[0].'<br>';


//Iteramos sobre la propiedad lenguajes del objeto
//------------------------------------------------
print 'lenguajes : ';
foreach ($info->lenguajes as $element) {
    printf("%s / ",$element);
}
print "<br>";

?>