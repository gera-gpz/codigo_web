<?php
// json_encode()  =  Variable Php  =>    Cadena Json


// De VariablePhp Array a Cadena Json
// ----------------------------------
$vector = array('manzana','naranja','platano');
$vcadena= json_encode($vector);

var_dump($vector);
var_dump($vcadena);


echo '<br><br><br><br><br><br>';


// De VariablePhp ArrayAsociativo a Cadena Json
// ---------------------------------------------
$asocia  = array('nombre'=>'Gerardo','apellido'=>'Pineda','depto'=>'Sistemas');
$acadena = json_encode($asocia);

var_dump($asocia);
var_dump($acadena);


echo '<br><br><br><br><br><br>';


// De VarablePhp ArrayNumerrico de Asociativo a Json
// -------------------------------------------------
$registros=array(array('Nombre'=>'Gerardo','Apellido'=>'Pineda','Depto'=>'Sistemas'),
                 array('Nombre'=>'Ramiro', 'Apellido'=>'Lopez', 'Depto'=>'Seguros'),
                 array('Nombre'=>'Sergio', 'Apellido'=>'Zamora','Depto'=>'Glosa'));
$rcadena=json_encode($registros);

var_dump($registros);
var_dump($rcadena);


echo '<br><br><br><br><br><br>';

$masocia =array( 'mex'=>array( 'pais'=>'mexico',
                              'idioma'=>'espanol',
                              'moneda'=>'peso'),
                'eua'=>array( 'pais'=>'eua',
                              'idioma'=>'ingles',
                              'moneda'=>'dolar'),
                'fra'=>array( 'pais'=>'francia',
                              'idioma'=>'ingles',
                              'moneda'=>'dolar')
              );
$objetoJson=json_encode($masocia);

var_dump($masocia);
var_dump($objetoJson);


?>