<?php
// Funcion json_decode()  
// Convierte:    Cadena Json   =>    Variable Php (array or object)


// VariablePhp String que contiene una cadena json
// -----------------------------------------------
$cadenaJson='{"numero":"10","nombre":"Gerardo","direccion":"Washington","telefono":"1000691",
              "lenguajes":["Sql","Html","Css","Javascript","Jquery","Php","Perl"]}';


//Se convierte la variablePhp string a array asociativo
//-----------------------------------------------------
$info = json_decode($cadenaJson,true);

echo 'numero    : '.$info['numero'].'<br>';
echo 'nombre    : '.$info['nombre'].'<br>';
echo 'direccion : '.$info['direccion'].'<br>'; 
echo 'telefono  : '.$info['telefono'].'<br>';


//Iteramos sobre la propiedad lenguajes del array asociativo
//----------------------------------------------------------
print 'lenguajes : ';
foreach ($info['lenguajes'] as $element) {
    printf("%s / ",$element);
}
print "<br>";
?>