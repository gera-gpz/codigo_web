<?php

require '../../../Libraries/ladybug/vendor/autoload.php';

$var = array(
    array(
        'name' => 'Raul',
        'age' => 29
    ),
    array(
        'name' => 'John',
        'age' => 27
    )
);

// ladybug_dump($var)

$ladybug = new \Ladybug\Dumper();
$ladybug->setTheme('modern');
echo $ladybug->dump($var);

?>