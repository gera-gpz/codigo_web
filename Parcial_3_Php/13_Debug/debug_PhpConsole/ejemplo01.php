<?php

require_once('../../../Libraries/php-console-master/src/PhpConsole/__autoload.php');


$a = 10;
echo 'Este es un ejemplo de PHP Console';
echo '<br><pre>';
var_dump($a);
echo '</pre>';


// Call debug from PhpConsole\Handler
$handler = PhpConsole\Handler::getInstance();
$handler->start();
$handler->debug('Debug $a = '.$a);


?>


