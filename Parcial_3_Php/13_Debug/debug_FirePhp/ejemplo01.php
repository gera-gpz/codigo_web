<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>

<?php

require_once('../../../Libraries/firephp-core-0.4.0/lib/FirePHPCore/FirePHP.class.php');

echo '<h1>This is the header</h1>';
$vector1=array('Gerardo','Fabian','Angel','Beto');
$vector2=array('Gerardo','Fabian','Angel','Beto',array('Juan','Patricio'));

//Instanciar clase con el constructor getInstance pasandole true
$firePhp = FirePHP::getInstance(true);


//metodos para mandar a consola:
$firePhp->log('Este menssaje es un log'); 
$firePhp->info('Este mensaje es un info'); 
$firePhp->warn('Este mensaje es un warn'); 
$firePhp->error('Este mensaje es un error'); 

$firePhp->log($vector1); 
$firePhp->log($vector2); 

?>

</body>
</html>
