<?php

$vint = 10;
$name = "Gerardo";

echo "<b>echo statement</b><br>";
echo "---------------<br>";
echo "Hello World <br><br><br><br>";

print "<b>print Statement</b><br>";
print "--------------- <br>";
print ("VariableEntera = $vint <br>");
print ("Tu nombre es : $name <br>");


$number = 9;
$str    = "Beijing";
$pi     = 3.1416;
printf("There are %u million bicycles in %s. <br>",$number,$str);
printf("Te value of Pi is %f <br><br>",$pi);


print "<b>sprint Statement</b><br>";
print "--------------- <br>";
$cad = sprintf("Tu nombre es %s ",$name);
print $cad;
?>


