const express = require('express');
const cors    = require('cors');
const mysql   = require('mysql2/promise');
const path = require('path');

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.text());



app.get('/usuario/', async (req,res) => { 
    const connection = await mysql.createConnection({host:'localhost', user: 'root', database: 'crud'});
    const [rows, fields] = await connection.execute('SELECT * FROM `cusuario`');
    res.json(rows);
});


app.get('/usuario/:id', async (req,res) => { 
    const connection = await mysql.createConnection({host:'localhost', user: 'root', database: 'crud'});
    const [rows, fields] = await connection.execute('SELECT * FROM `cusuario` WHERE `idUsuario` = ?', [req.params.id]);
    
    if (rows.length==0) {
        res.json({registros:"No se encontro usuario"});
    } else {
        res.json(rows);
    }
});


app.post('/usuario/', async (req,res) => {
    let sentenciaSql=`insert into cusuario values(${req.body.idUsuario},'${req.body.nombre}','${req.body.apPaterno}','${req.body.apMaterno}','${req.body.login}','${req.body.password}')`;
    const connection = await mysql.createConnection({host:'localhost', user: 'root', database: 'crud'});
    const [rows, fields] = await connection.execute(sentenciaSql);
    if (rows.affectedRows==1) {
        res.json({status:"Insercion Realizada con exito"});
    }
});

app.delete('/usuario/:id', async (req,res) => { 
    const connection = await mysql.createConnection({host:'localhost', user: 'root', database: 'crud'});
    const [rows, fields] = await connection.execute('DELETE FROM `cusuario` WHERE `idUsuario` = ?', [req.params.id]);
    
    res.json(rows);
    if (rows.affectedRows==1) {
        res.json({status:"Registro Eliminado"});
    }
});


app.put('/', async (req, res) => {
    const sql = `update cusuario set nombre ='${req.body.nombre}',apPaterno='${req.body.apPaterno}',apMaterno='${req.body.apMaterno}',login=${req.body.login},password=${req.body.password} where idEmpleado=${req.body.IdUsuario}`; 
    const con = await mysql.createConnection({host:'localhost', user: 'root', database: 'crud'});
    const [rows, fields] = await con.execute(sql);
    if (rows.affectedRows==1) { 
            res.status(200).send(`Usuario Modificado`);
    } else {
            res.status(500).send(`Usuario No Modificado`);
    }
});


app.use((req,res)=>{ 
    res.status(404).json({estado:"Pagina=Ruta No Encontrada"})
});

app.listen(8082,()=>{  
    console.log("Servidor Express corriendo y escuchando en el puerto 8082")
});
