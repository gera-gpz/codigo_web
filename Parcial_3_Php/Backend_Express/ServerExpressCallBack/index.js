const express   = require('express');
const cors      = require('cors');
const mysql2    = require('mysql2');
const mysql2p   = require('mysql2/promise');
const { jsPDF } = require("jspdf"); 
const path      = require('path');

const app = express();
app.use(cors());

const connection = mysql2.createConnection({
    host:     'localhost',
    user:     'root',
    database: 'crud'
});


app.get('/usuarios/formato',(req,res)=>{
    const doc = new jsPDF();
    doc.text("Hello world!", 10, 10);
    let archivoPDF=path.join(__dirname,'a4.pdf');
    doc.save(archivoPDF);
    res.sendFile(archivoPDF);
})
    


app.get('/usuarios',(req,res)=>{
    let sentenciaSql='';
    if (typeof(req.query.idUsuario)=='undefined') {
        sentenciaSql = `select * from cusuario`;
    } else {
        sentenciaSql = `select * from cusuario where idUsuario = ${req.query.idUsuario}`;
    }
    console.log(sentenciaSql);

    connection.query(sentenciaSql,function(err,results,fields) {
        
        if (results.length==0) {
            res.json({ status : 0,
                       mensaje: "Usuario no existe",
                       datos  : [{}] });
        } else {
            res.json({ status : 1,
                       mensaje: "Usuario encontrado", 
                    // datos  : (results.length==1)  ? results[0]  : results });
                       datos  : results });
        }
    });
});





app.delete('/usuarios',(req,res)=>{ 
    console.log(req.query.idUsuario);
    let sentenciaSql='';
    if (typeof(req.query.idUsuario)=='undefined') {
        res.json({ status : 0,
                   mensaje: "Falto enviar el Id de Usuario",
                   datos  : [{}] });
    } else {
        sentenciaSql = `delete from cusuario where idUsuario = ${req.query.idUsuario}`;
    }
    
    connection.query(sentenciaSql,function(err,results,fields) {
        if (results.affectedRows==1) {
            res.json({ status : 1,
                       mensaje: "Registro Eliminado",
                       datos  : [{}] });
        } else {
            res.json({ status : 0,
                       mensaje: "No se pudo eliminar",
                       datos  : [{}] });
        }
    });
});



app.post('/usuarios',(req,res)=>{ 
    res.json({ mensaje:"Server Express respondiendo a post"});
});



app.patch('/usuarios',(req,res)=>{ 
    res.json({ mensaje:"Server Express respondiendo a delete"});
});



app.listen(8082,(req,res)=>{
    console.log("Servdidor express corriendo en puerto 8082")
});
