$(document).ready(function() { 

   var alturaOrig = $('.menu').offset().top;  //distancia entre menu y parte alta de la pag
    
    console.log("alturaOrig = "+alturaOrig);    
    
    $(window).on('scroll',function() {
        
        var alturaScroll= $(window).scrollTop();
        
        console.log("alturaScroll ="+alturaScroll);
        
        if (alturaScroll > alturaOrig) 
            { $('.menu').addClass('menufixed'); }
        else    
            { $('.menu').removeClass('menufixed');}
        
    });
});