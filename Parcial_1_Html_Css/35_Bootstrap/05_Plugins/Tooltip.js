
$('document').ready( function() {
    
    /* For Performance reasons (In Bootstrap)
       Tooltips needs to be initialized through JQuery
    */
   
    var options = { animation:true };
    
    $('.tooltipButton').tooltip(options);

});