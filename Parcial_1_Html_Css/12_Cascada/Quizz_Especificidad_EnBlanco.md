
## 1 Cuál es el valor de especificidad de este selector: `p.parrafo` ?  
- 12
- **[11]**
- 101
- 110

<br>
<br>
<br>

## 2 Cuál es el valor de especificidad de este selector: `div.contenedor > p` ?  
- 11
- 22
- **[12]**
- 102

<br>
<br>
<br>

## 3 Cuál es el valor de especificidad de este selector: `div#par1 > p` ?  
- 21
- 11
- 12
- **[102]**

<br>
<br>
<br>

## 4 Cuál es el valor de especificidad de este selector: `ul > .menu > li .opcion` ?  
- **[22]**
- 102
- 12
- 112

<br>
<br>
<br>

## 5 Cuál es el valor de especificidad de este selector: `ol#nav li.active a` ?  
- 25
- 110
- **[113]**
- 112

<br>
<br>
<br>

## 6 Que selector usaríamos para seleccionar los elementos `<p>` que esten dentro de una clase `.cont`?
- `p .cont`
- `.cont + p`
- **`[.cont p]`**

<br>
<br>
<br>

## 7 Que selector usaríamos para seleccionar los elementos `<p>` que tengan el id `#par`  
##   y que esten dentro de una clase `.contenedor`?  
- `.contenedor #par p`
- **`[.contenedor p#par]`**
- `.contenedor p #par`

<br>
<br>
<br>

## 8 Que selector usaríamos para seleccionar los elementos `<p>` con la clase `.par`
##   y que desciendan directamente de una clase `.contenedor`?  
- **`[.contenedor > p.par]`**
- `.contenedor > .par p`
- `p.par > .contenedor`

<br>
<br>
<br>

## 9 Que selector usaríamos para seleccionar los elementos `<p>` que sean hermanos adyacentes de un `<div>`  
##   con la clase `.contenedor`?  
- **`[div.contenedor + p]`**
- `div.contenedor ~ p`
- `p + div.contenedor`

<br>
<br>
<br>

## 10 De que color se pintaria el párrafo?
- Green
- **[Red]**
- Blue

