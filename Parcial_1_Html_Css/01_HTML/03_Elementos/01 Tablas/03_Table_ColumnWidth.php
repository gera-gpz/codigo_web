<!DOCTYPE HTML>
<html>

<head>
    <title>Fixed Headers</title>
    <link rel="stylesheet" href="03_Table_ColumnWidth.css"/>
</head>

<body>  
    <table class="users">
    <thead>
        <tr><th class="col1">Column 1</th>
            <th class="col2">Column 2</th>
            <th class="col3">Column 3</th>
            <th class="col4">Column 4</th>
        </tr>
    </thead>
    <tbody>

        <?php
        for($x=1; $x<=20; $x++) {
        echo "<tr>";
        echo "<td class='col1'>Column 1</td>";
        echo "<td class='col2'>Column 2</td>";
        echo "<td class='col3'>Column 3</td>";
        echo "<td class=col4>Column 4</td>";
        echo "</tr>";
        }
        ?>

    </tbody>
    </table>    

</body>

</html>