// Ejemplos de declaración de variables en Javascript



// -----------------------------------------------------------------
// 1. La variable declarada con let no la veremos fuera del bloque {}
// -----------------------------------------------------------------
if (true) {
    let saludo = 'hola';
}
console.log(saludo);



// ----------------------------------------------------------------------
// 2. Una variable declarada con var si será accesible fuera del bloque {}
// ----------------------------------------------------------------------
if (true) {
    var saludo = 'hola';
}
console.log(saludo);


// -------------------------------------------------------------------------------------
// 3. La variable declarada con let dentro de una función no será accesible fuera de ella
// -------------------------------------------------------------------------------------
function Hola() {
    let saludo = 'hola';
}
Hola();
console.log(saludo);

// -------------------------------------------------------------
// 4. El ámbito de las variables declaradas con var es de función
// -------------------------------------------------------------
function Hola() {
    var saludo = 'hola';
}
Hola();
console.log(saludo);


// ---------------------------------------------------------------------------------------------
// 5. Si dentro de una función omitimos la palabra clave var/let, la variable pasará a ser global
// ----------------------------------------------------------------------------------------------

function Hola() {
    saludo = 'hola';
}
Hola();
console.log(saludo);



// ------------------------------------------------------------------------------------
// 6. Hoisting(Elevación) las declaraciones con var son elevadas al inicio de la función
// ------------------------------------------------------------------------------------
function saludar() {
    frase = 'hola';
    console.log(frase);
    var frase;
}
saludar();


// ------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------
// 7. Hoisting. Solamente se elevará la declaración, la asigación se quedará donde este.
// ------------------------------------------------------------------------------------
    function saludar() {
        console.log(frase);
        var frase='Hola';
    }
    saludar();


// ----------------------------------------------------------------------------------------------
// 8 Hoisting. La elevación se hace incluso si la declaración sucede dentro de un bloque de código
// ----------------------------------------------------------------------------------------------
function saludar() {
    frase='Hola';
    console.log(frase);
    if (false) {
        var frase;
    }
}
saludar();


// ------------------------------------------------------------------------------------
// 9 La segunda variable declarada con let solo será visible dentro de su propio bloque
// ------------------------------------------------------------------------------------
let vers = '1';
console.log(vers);

if (true) {
    let vers = '2';
    console.log(vers);
}
console.log(vers);


// ----------------------------------------------------
// 10 Ambito de función en variables declaradas con var
// ----------------------------------------------------
var ver = '1';
console.log(ver);

function miFuncion() {
    var ver = '2';
    console.log(ver);
}

miFuncion();
console.log(ver);
