// FUNCIONES JAVASCRIPT

// -------------------------------------------------------------------------------------------------
// Declaración de función
// -------------------------------------------------------------------------------------------------

function saludar(nombre) {
    console.log(`Hola ${nombre}.`);
}
saludar('Gerardo');


// A las declaraciones de función se les hace Hoisting
// ---------------------------------------------------

saludar('Gerardo');
function saludar(nombre) {
    console.log(`Hola ${nombre}.`)
}


// Por defecto las funciones regresan undefined
// ---------------------------------------------
function saludar(nombre) {
    console.log(`Hola ${nombre}.`)
}
console.log(saludar('Gerardo'));


// Si queremos que la función regrese algo debemos especificarlo con return
// ----------------------------------------------------------------
function saludar(nombre) {
    console.log(`Hola ${nombre}.`)
    return 1;
}
console.log(saludar('Gerardo'));


// -------------------------------------------------------------------------------------------------
// Expresion de función
// -------------------------------------------------------------------------------------------------

var salute1 = function miFuncion(nombre) {
    console.log(`Hola ${nombre}.`)
}
salute1('Gerardo');


// En una expresión de función, La función puede ser anónima
// ----------------------------------------------------------
var salute2 = function(nombre) {
    console.log(`Hola ${nombre}.`)
}
salute2('Gerardo');



// A las expresiones de función no se les hace hoisting, (Usarlas antes de declararlas generará Error)
// ----------------------------------------------------------------------------------
console.log(saludar);               // Undefined
var saludar = function(nombre) {
    console.log(`Hola ${nombre}.`)
}


saludar('Gerardo');                 // TypeError: saludar is not a function
var saludar = function(nombre) {
    console.log(`Hola ${nombre}.`)
}


saludar('Victor');                   // ReferenceError: saludar is not defined
let saludar = function(nombre) {
    console.log(`Hola ${nombre}.`)
}









// -------------------------------------------------------------------------------------------------
// IIFE Immediately Invoked Function Expression
// -------------------------------------------------------------------------------------------------
// Ejecutar este código no hará absolutamente nada:
function saludar(nombre) {
    console.log(`Hola ${nombre}.`)
}


// Pero si envolvemos la función entre paréntesis, seguida de ()
// La convertiremos en una IIFE
(function saludar(nombre) {
    console.log(`Hola ${nombre}.`)
})('Gera');



// No podremos acceder a las variables dentro del IIFE
(function saludar(nombre) {
    var flag=10;
    console.log(`Hola ${nombre}.`)
})();
console.log(flag);          //Reference Error



// Una IIFE Puede ser función anómina:
( function() {
    var cadena="Hola Mundo"
    console.log(cadena);
    return cadena;
})();

// Una IIFE Puede ser función flecha:
( () => {
    var cadena="Hola Mundo"
    console.log(cadena);
    return cadena;
})();



// Si necesitamos el valor de retorno de una IIFE
// Lo guardamos en una variable
//----------------------------------
let miCadena1= ( function() {
    var cadena1="Hola Mundo"
    //console.log(cadena);
    return cadena1;
})();


// Al asigar la IIFE a una variable no necesitamos
// encerrarla entre parentesis
//------------------------------------------------
let miCadena2 = function() {
    var cadena2="Hola Mundo"
    console.log(cadena2);
    return cadena2;
}();



// No confundir "Expresión de Funcion" con una "IIFE"
//---------------------------------------------
// Expresion de Funcion Anónima
var sumar1 = function (a,b) {
    return a+b;
}
console.log(sumar1);
console.log(sumar1());
console.log(sumar1(2,2));


// IFFE Anónima
var sumar2 = function (a,b) {
    return a+b;
}(1,2);
console.log(sumar2);




// -------------------------------------------------------------------------------------------------
// Funcion Flecha
// -------------------------------------------------------------------------------------------------

var saludar = (nombre) =>  { return "Hola " + nombre; }
console.log(saludar("Gerardo"));

var saludar = (nombre) => "Hola " + nombre; 
console.log(saludar("Gerardo"));







// -------------------------------------------------------------------------------------------------
// Shorthand
// -------------------------------------------------------------------------------------------------
//Ejemplo de declaracion de una clase y sus metodos de manera "normal"
//--------------------------------------------------------------------
const saludos = {
    persona: [],
    agregar: function(persona) {
      this.persona.push(persona);
    },
    saludar: function(index) {
      return `Hola ${this.persona[index]}`;
    }
  };

saludos.agregar('Gerardo');
saludos.agregar('Fernanda');
console.log(saludos.saludar(1));


//Ejemplo de declaracion de la misma clase y sus metodos de manera "abreviada"
//----------------------------------------------------------------------------
const saludos = {
    persona: [],
    agregar(persona){ this.persona.push(persona); },
    saludar(index)  {return `Hola ${this.persona[index]}`;}
  };

saludos.agregar('Gerardo');
saludos.agregar('Fernanda');
console.log(saludos.saludar(1));






// -------------------
// Generador de Funcion
// -------------------

function *generador() {
    var contador = 1;
    yield contador++;
    yield contador++;
}

var g = generador() ;
console.log ( g.next().value ); // 1
console.log ( g.next().value ); // 2





function *generador() {                    // Se declara la función con *
         console.log('Inicio Función');
         yield 'Hola ';                    // yield = Salir // value = 'Hola'
         console.log('Continua Función');
         yield 'Mundo!';                   // yield = Salir // value = 'Mundo'
}

const gen = generador();                   // Se asigna la función a una constante
console.log(gen.next().value);             // La invocamos por primera vez y comienza su ejecución
console.log('Función en pausa');           // Ejecutamos código fuera de la fucnion
console.log(gen.next().value);             // Regresamos a la ejecucion de la funcion



// -------------------
// Constructor de Función
// -------------------

// En Javascript las funciones las podemos construir en tiempo de ejecucion

const sumar = new Function (`a`,`b`,`return a+b`);
console.log(sumar(5,5));


const saludar = new Function (`nombre`,"return `Hola ${nombre}.`");
console.log(saludar('Gerardo'));





























// -------------------------------------------------------------------------------------------------
// Shorthand
// -------------------------------------------------------------------------------------------------
//Ejemplo de declaración de una clase y sus metodos de manera "normal"
//--------------------------------------------------------------------
const saludos1 = {
    persona: [],
    agregar: function(persona) {
      this.persona.push(persona);
    },
    saludar: function(index) {
      return `Hola ${this.persona[index]}`;
    }
  };

saludos1.agregar('Gerardo');
saludos1.agregar('Fernanda');
console.log(saludos1.saludar(1));


//Ejemplo de declaracion de la misma clase y sus metodos de manera "abreviada"
//----------------------------------------------------------------------------
const saludos2 = {
    persona: [],
    agregar(persona){ this.persona.push(persona); },
    saludar(index)  { return `Hola ${this.persona[index]}`;}
  };

saludos2.agregar('Gerardo');
saludos2.agregar('Fernanda');
console.log(saludos2.saludar(1));










// -------------------
// Generador de Función
// -------------------
function *generador() {
    var contador = 1;
    yield contador++;
    yield contador++;
}

var g = generador();
console.log ( g.next().value ); // 1
console.log ( g.next().value ); // 2





function *generador() {                    // Se declara la función con *
         console.log('Inicio Función');
         yield 'Hola ';                    // yield = Salir // value = 'Hola'
         console.log('Continua Función');
         yield 'Mundo!';                   // yield = Salir // value = 'Mundo'
}

const gen = generador();                   // Se asigna la función a una constante
console.log(gen.next().value);             // La invocamos por primera vez y comienza su ejecución
console.log('Función en pausa');           // Ejecutamos código fuera de la fucnion
console.log(gen.next().value);             // Regresamos a la ejecucion de la funcion









// -------------------
// Constructor de Función
// -------------------
// En Javascript las funciones las podemos construir en tiempo de ejecución

const sumar = new Function (`a`,`b`,`return a+b`);
console.log(sumar(5,5));


const saludar = new Function (`nombre`,"return `Hola ${nombre}.`");
console.log(saludar('Gerardo'));

