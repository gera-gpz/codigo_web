//  Funciones de Orden Superior:
//
//  Aceptan funciones como parámetros

let vector = ["Gerardo", "Alberto", "Gustavo", "Sergio", "Gilberto"];

let vectorg = vector.filter((elemento) => {
  if (elemento[0] === "G") {
    return true;
  } else {
    return false; 
  }
});


let vectorgg = vector.filter(elem => elem[0]==='G');

console.log(vector);
console.log(vectorg);
console.log(vectorg);


//  Funciones de Orden Superior:
//
//  Las funciones que pasamos como parametros las podemos reutilizar 


let vec = ['Gerardo','Veronica','Gloria','Jose','Geraldine','Gustavo','Griselda'];

function primeraLetraEsG(e) {
    return ( e[0] === 'G')
}

function segundaLetraEsE(e) {
    return ( e[1] === 'e')
}

let vecGe = vec.filter(primeraLetraEsG).filter(segundaLetraEsE)

console.log(vecGe)
