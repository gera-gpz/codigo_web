
//Displays and alert box after 3000 miliseconds
setTimeout(function() { console.log('Hello'); },3000);


// Print the numbers from 1 to 10, 100ms apart. Or not.

// for(var i = 0; i < 10; i++){
// 	setTimeout(function() {console.log(i+1);}, 100*i);
// }




/* To fix the bug, wrap the code in a self-executing function expression:*/

for(var i = 0; i < 10; i++){

	(function(i){
		setTimeout(function() { console.log(i+1); }, 100*i);
	})(i);
}
