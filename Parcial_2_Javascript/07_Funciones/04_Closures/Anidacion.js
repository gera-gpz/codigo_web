// Ejemplos de funciones anidadas


//---------
//Ejemplo 1
//---------

function obtenerSuma () {
  var num1 = 2, num2 = 3;

  function suma() {           // Esta función esta anidada
    return (num1 + num2);
  }
  return suma();
}

var suma = obtenerSuma();     // Retorna 5
console.log(suma);





//---------
//Ejemplo 2
//---------

function addSquares(a,b) {
  function square(x) {
    return x * x;
  }
  return square(a) + square(b);
}
a = addSquares(2,3); // retorna 13
b = addSquares(3,4); // retorna 25
c = addSquares(4,5); // retorna 41

console.log(a,b,c);





//---------
//Ejemplo3
//---------

function externa(x) {
    function interna(y) {
      return x + y;
    }
    return interna;         // La función externa regresa la función externa
}

fn_interna = externa(3);    // Le mandamos un 3 a la funcion externa esto nos regreara la función interna
resultado1 = fn_interna(5);  // retorna 8
console.log(resultado1);

resultado2 = externa(3)(5);  // retorna 8
console.log(resultado2);