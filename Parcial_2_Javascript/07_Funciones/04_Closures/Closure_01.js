// Cada que se crea una funcion se crea un closure, cada closure tiene:
// - Su scope local
// - El scope de su función padre
// - El scope global


var hola = "Hola";                      //Contexto de ejecución global

var imprimirHolaMundo = function() {
        var mundo = "Mundo";            //Contexto de ejecucion de imprimirHolaMundo()
        
        function imprime() {
                var anio = "2022";      //Contexto de ejecucion de imprime()
                console.log(hola + " " + mundo + " "+ anio);
        }
        return imprime;
}

var llamarHola = imprimirHolaMundo();
llamarHola();


// La funcion interna imprime() es retornada desde la funcion externa 
// antes de ser ejecutada


// Un uso común de los closures
// es emular variales y métodos privados
// ya que Javascript no los soporta nativamente