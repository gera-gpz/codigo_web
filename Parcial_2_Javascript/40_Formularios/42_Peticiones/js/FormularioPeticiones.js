$(document).ready(function() {


      // Cargando el Header del servidor XMLHttpRequest
      // ----------------------------------------------
      $('#btnHeaderAjax').click(function() {
            var solicitud = new XMLHttpRequest();
            solicitud.onreadystatechange = function() {
            if (solicitud.readyState == 4 && solicitud.status == 200) {
                  document.getElementById("enca").innerHTML = solicitud.responseText;
            }};
            solicitud.open("GET","./php/getHeader.txt", true);
            solicitud.send();
      });
      // Cargando el Header del servidor Envolviendo XMLHttpRequest en una Promesa
      // -------------------------------------------------------------------------
      $('#btnHeaderAjaxP').click(function() {
            // 1) Instanciamos el objeto promesa que recibe una función con dos callbacks
            // uno para resolver la promesa y otro para rechazarla
            let promesa = new Promise(function(resolve,reject) { 
                  var solicitud = new XMLHttpRequest();
                  solicitud.onreadystatechange = function() {
                  if (solicitud.readyState == 4 && solicitud.status == 200) {
                        resolve(solicitud.responseText);
                  }};
                  solicitud.open("GET","./php/getHeader.txt", true);
                  solicitud.send();
            });

            // 2) Consumimos la promesa. El objeto promesa tiene un método then()
            // Dentro del metodo then() colocamos la función que procesará la respuesta
            // El parámetro que recibe esta función es la respuesta de la promesa
            promesa.then( value => document.getElementById("enca").innerHTML = value );
      });
      // Boton para devolver el header a su estado original
      //---------------------------------------------------
      $('#btnHeaderReset').click(function() {
            document.getElementById("enca").innerHTML = "Catálogo de clientes";
      });




      // Boton para Obtener hora del servidor usando XMLHttpRequest
      // ----------------------------------------------------------
      $('#btnHoraAjax').click(function() {
            var solicitud = new XMLHttpRequest();
            solicitud.onload = function() {
                  document.getElementById("hora").value=solicitud.responseText;
            }
            solicitud.open("GET","./php/getHora.php",true);
            solicitud.send();
      });
      // Boton para obtener hora del servidor usando JQuery
      // --------------------------------------------------
      $('#btnHoraJQuery').click(function() {
            $.get( "./php/getHora.php", function(vhora) {
                  $("#hora").val(vhora);
            });
      });
      // Boton para obtener hora del servidor usando API Fetch
      // --------------------------------------------------
      $('#btnHoraFetch').click(function() {
            fetch('./php/getHora.php')
               .then(resp=>resp.text())
                  .then(vhora=>$("#hora").val(vhora));
      });
      // Boton para obtener hora del servidor usando API Fetch + Async/Await
      // --------------------------------------------------
      $('#btnHoraFetchAA').click( async function() {
            let resp  = await fetch('./php/getHora.php');
            let vhora = await resp.text();
            $("#hora").val(vhora);
      });





      // Boton para obtener un registro JSON del servidor usando JQuery
      // --------------------------------------------------------------
      $('#btnJsonJQuery').click(function() {
            $.post('./php/GetRegistro.php', function(dato) {
                  actualizaFormulario(dato)}, 
                  'json');
            });
      // Boton para obtener un registro JSON del servidor usando API Fetch
      // -----------------------------------------------------------------
      $('#btnJsonFetch').click(function() {
            fetch('./php/getRegistro.php')
                  .then(respuesta => respuesta.json())
                      .then(function(dato) { actualizaFormulario(dato) });
      });

      // Funcion que recibe un objeto con los datos del formulario y refresca
      // --------------------------------------------------------------------
      function actualizaFormulario(objeto) {
            console.log(objeto);
            $('#idCliente').val(objeto.idcliente);
            $('#nombre').val(objeto.nombre);
            $('#hora').val(objeto.hora);
            $('#direccion').val(objeto.direccion);
            $('#telefono').val(objeto.telefono);
            $('#ciudad').val(objeto.ciudad);
            $('#estado').val(objeto.estado);
            $('#pais').val(objeto.pais);
      }
});
