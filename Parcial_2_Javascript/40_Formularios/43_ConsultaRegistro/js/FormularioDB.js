$(document).ready(function () {
  
  // Pedir parametro para consulta con la funcion prompt de Javascript
  // ---------------------------------------------------------------
  $("#btnJQuery1").click(function () {
    let id = prompt("Ingrese el id cliente:");

    $.post("./php/getRegistroDB.php",{ par: id },function (registro) {
        actualizaFormulario(registro);
    },"json");
  });
  // Pedir parametro para consulta con un modal de Boostrap
  // ---------------------------------------------------------------
  $("#btnJQuery2").click(function () {
    $("#modal1").modal("show");
  });

  $("#btnModalCerrar,#btnModalCancelar").click(function () {
    $("#modal1").modal("hide");
  });

  $('#btnModalConsultar').click(function() {
        $('#modal1').modal('hide');
        let id = $('#idconsulta').val();
        $.post('./php/getRegistroDB.php',{par:id},function(registro){
          actualizaFormulario(registro);
        },'json');
  });


  // Pedir parametro para consulta con la funcion prompt() de Javascript
  // y hacer fetch para traer los datos
  // ---------------------------------------------------------------
  $("#btnFetch").click(function () {
    
    let id = prompt("Ingrese el id cliente:");
    let datosaEnviar = new FormData();
    datosaEnviar.set('par',id);

    let opcionesFetch = {
      method: "POST",
      body   : datosaEnviar
    }

    fetch("./php/getRegistroDB.php", opcionesFetch)
      .then((respuesta) => respuesta.json())
        .then(function (dato) { actualizaFormulario(dato) });
    });


  // Actualiza formulario
  // ---------------------    
  function actualizaFormulario(objeto) {
    console.log(objeto);
    $("#idCliente").val(objeto.idcliente);
    $("#nombre").val(objeto.nombre);
    $("#hora").val(objeto.hora);
    $("#direccion").val(objeto.direccion);
    $("#telefono").val(objeto.telefono);
    $("#ciudad").val(objeto.ciudad);
    $("#estado").val(objeto.estado);
    $("#pais").val(objeto.pais);
  }
});
