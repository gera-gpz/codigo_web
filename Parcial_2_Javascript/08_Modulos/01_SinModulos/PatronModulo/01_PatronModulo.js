
// -----------------------
// Patron Moddulo Revelador
// -----------------------

var moduloUsuario = (function() {
    var nombre  = "Gerardo";
    var saludo  = "Hola Mundo";

    function asignaNombre(parNombre) {    // Metodo publico
            nombre = parNombre;
    }
    
    function imprimirNombre() {           // Método privada
        console.log("Nombre:" + nombre);
    }
     
    return {                              // Funciones y Variables Publicas
        asignaNombre: asignaNombre,       // Son "expuestas" en un objeto
        mensaje: saludo
    };
})();

moduloUsuario.asignaNombre("Carlos");
moduloUsuario.mensaje="Hola";