
export function pasarMayusculas(cadena) {
    return cadena.toUpperCase();
}

export function quitarEspacios(cadena) {
    return cadena.trim();
}

export function obtenerLongitud(cadena) {
    return cadena.length;
}
