// JSON.stringify() 
// Convierte un objeto a cadena
// -----------------------------------------------------------
let objetoe = { 
    nombre:    "Gerardo", 
    apellidop: "Pineda",
    apellidom: "Zapata",
}
let cadenae = JSON.stringify(objetoe);

console.log('JSON.stringify() De objeto->cadena');
console.log('objetoe es de tipo '+typeof(objetoe)+' y su valor es '+objetoe);
console.log('cadenae es de tipo '+typeof(cadenae)+ ' y su valor es '+cadenae);
console.log('');



// JSON.parse()
// Convierte una cadena a objeto Javascript
// -----------------------------------------------------------
console.log(`\nJSON.parse() De cadena->objeto\n`);
let cadena = `{ "nombre":"Gerardo", "apellidop":"Pineda", "apellidom":"Zapata" }`;
let objeto = JSON.parse(cadena);

console.log('cadenaReg es de tipo '+typeof(cadenaReg)+' y su valor es '+cadena);
console.log('registro es de tipo '+typeof(registro)+ ' y su valor es '+objeto);









const rawJSON = JSON.rawJSON("12345678901234567890");
JSON.stringify({ value: rawJSON });








// let miVariableInteger = 10;
// let miVariableString=JSON.stringify(miVariableInteger);

// console.log('El tipo de miVariableInteger es '+typeof(miVariableInteger));
// console.log('El tipode miVariableString es '+typeof(miVariableString));
