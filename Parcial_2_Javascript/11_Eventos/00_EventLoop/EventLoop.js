
const proceso1 = () => console.log("Proceso 1");
const proceso2 = () => setTimeout(() => console.log("Proceso2"), 500);
const proceso3 = () => console.log("Proceso 3");

proceso2();
proceso1();
proceso3();