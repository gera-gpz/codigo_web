$(document).ready(asignarEventos);

function asignarEventos() {
    $("#menu a").click(presionEnlace);
}

function presionEnlace() {
    var pagina = $(this).attr("href"); //extrae el atributo href del disparador

    $(document).ajaxStart(barraEstatusI);

    $("#detalles").load(pagina); //load  1) establece comunicacion asincrona
                                 //      2) espera y recibe los datos
                                 //      3) anade la informacion al selector
    $(document).ajaxStop(barraEstatusT);
    return false;
}

function barraEstatusI() {
    $("#estatus").html("Iniciando....");
}

function barraEstatusT() {
    $("#estatus").html("Termino......");
}
