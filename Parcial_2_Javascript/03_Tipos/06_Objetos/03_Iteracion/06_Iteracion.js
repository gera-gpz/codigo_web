

// ------------------------------------------
// Iterando sobre las propiedades de un objeto
// ------------------------------------------

let objeto = {
    nombre:    "Gerardo",
    apellidop: "Pineda",
    apellidom: "Zapata"
} 

let prop = Object.keys(objeto);

prop.forEach( (propiedad)=>{ console.log(propiedad); }  )


let props = Object.getOwnPropertyNames(objeto);

props.forEach( (propiedad)=>{ console.log(propiedad); }  )
