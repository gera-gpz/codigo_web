let objeto = {
    nombre:    "Gerardo",
    apellidop: "Pineda",
    apellidom: "Zapata"
} 


for (let propiedad in objeto) {
    console.log(propiedad);              // Accedemos a la propiedad
    console.log(objeto[propiedad]);      // Accedemos al valor con la notación de corchetes
} 



for (let propiedad in objeto) {
     if (objeto.hasOwnProperty(propiedad) {  // Metodo hasOwnProperty ignora propiedades en caena de prototipos 
        console.log(propiedad);              // Accedemos a la propiedad
        console.log(objeto[propiedad]);      // Accedemos al valor con la notación de corchetes
    }
}
