
let objeto = {
    control: 1,
    nombre:  "Gerardo",
    paterno: "Pineda",
    materno: "Zapata"
}


let propiedades = Object.keys(objeto);
let valores     = Object.values(objeto);
let entradas    = Object.entries(objeto);


console.log(propiedades);
console.log(valores);
console.log(entradas);


propiedades.forEach( (prop)=>{ console.log(prop) })

valores.forEach( (val)=>{ console.log(val) })

entradas.forEach( ( [prop,val])=>{ console.log(prop+" => "+val) })

entradas.forEach( ([prop,valor])=>{ console.log(` ${prop} = ${valor} `); }  )



