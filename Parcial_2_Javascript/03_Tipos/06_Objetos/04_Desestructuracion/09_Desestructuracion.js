
// La Desestructuracion nos permite estraer las propiedades de un objeto a variables

// En lugar de hacer esto :

let alumno = {
    control:93100095,
    nombre:'Gerardo',
    apellido:'Pineda',
    edad:20,
}

// Fora clasica de asignar las propiedades de un objeto a variables
let nom=alumno.nombre;
let ape=alumno.apellido;

console.log(nom+' '+ape)

// Usando la desestructuracion):  
let {nombre,apellido} = alumno;
console.log(nombre+' '+apellido);

// Usando la desestructuracion y asignar la propiedad a un nuevo nombre de variable):  
let {nombre:no,apellido:ap} = alumno;
console.log(no+' '+ap);

// Usando el operador spread para obtener el resto de parametros
let {nombre:vnom,apellido:vape,...vres} = alumno;
console.log(vnom,vape,vres);
