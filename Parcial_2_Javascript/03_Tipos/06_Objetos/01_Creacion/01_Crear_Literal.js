// ------------------------------------------
// Creacion de Objetos (Literales)
// ------------------------------------------

let alumno = {};               // Objeto literal vacío
alumno.nombre   = "Ernesto";   // Agregamos propiedades y sus valores
alumno.apellido = "Zedillo";

console.log(typeof(alumno));
console.log(alumno);


let maestro = {                // Objeto literal con propiedades y valores iniciales
    nombre:   "Jose",       
    apellido: "Castro",        // Coma opcional a la última propiedad (trailing coma) 
}
console.log(typeof maestro );
console.log(maestro);




var cliente = {

  nombre: "Juan",
  deposito: 0,

  imprimir: function () {
    console.log(this.nombre);
    console.log(this.deposito);
  },

  depositar: function (monto) {
    this.deposito = this.deposito + monto;
  },

  retirar: function (monto) {
    this.deposito = this.deposito - monto;
  },
};


cliente.imprimir();

cliente.depositar(1000);
cliente.imprimir();

cliente.depositar(500);
cliente.imprimir();

cliente.retirar(250);
cliente.imprimir();
