
var Empleado = {                              // Definimod un objeto "prototipo"
    departamento: "Eventual",                 // Valor default para propiedad departamento  
    getDepartamento: function () {            // Metodo para mostrar la propiedad
          console.log(this.departamento);
     }
};
  
  
let empleadoEve = Object.create(Empleado);     // Crea objeto empleadoEve su prototipo sera Empleado
empleadoEve.getDepartamento();                 // Muestra Eventual
console.log(empleadoEve.__proto__);            // Mostrara el objeto Empleado

let empleadoSis = Object.create(Empleado);      // Crea objeti empleadoSis su prototipo sera Empleado
empleadoSis.departamento = "Sistemas";          // Asigna valores para su propiedad
empleadoSis.getDepartamento();                  // Muestra Sistemas