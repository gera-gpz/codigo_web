// ----------------------------------
// Una variable puede cambiar de tipo
// ----------------------------------

let variable = "Juan";    // Declaración de variable y asignación de un string
console.log(variable);

variable = 5000;          // Cabiamos su valor (su tipo) a number
console.log(variable);

variable = "Fernando";    // Cabiamos su valor (su tipo) a string
console.log(variable);



// ---------------------------------------------------------------------------------------------
// Operador typeof para saber el Tipo de dato de una variable, isInteger para saber si es entero
// ---------------------------------------------------------------------------------------------

let miVar;
console.log('El tipo de miVar es... '+typeof(miVar));

miVar="Sistemas";
console.log('El tipo de miVar es... '+typeof(miVar));

console.log('miVar es Integer?... '+Number.isInteger(miVar));

variable = 5000;
console.log(typeof(variable));



// ---------------------------------------
// Tipos Primitivos = Asignacion por valor
// ---------------------------------------
let va = 1;
let vb = a;
console.log(va,vb);

vb=2;
console.log(va,vb);


// -----------------------------------------------
// Tipos No-primitivos = Asignacion por referencia
// -----------------------------------------------
let oa = [1,2,3];
let ob = a;

console.log(oa,ob);

ob.push(4);
console.log(oa,ob);

// --------------------------------------------------
// Pasando Primitivos a una funcion, se pasa el valor
// --------------------------------------------------
function sumaUno(arg) {
    arg = arg + 1;
    return arg;
}

let a = 1;
let b= sumaUno(a);

console.log(a);
console.log(b);


// ----------------------------------------------------
// Pasando objetos a una funcion, se pasa la referencia
// ----------------------------------------------------
function agregaElemento(arg) {
    arg.push(4)
    return arg;
}

let a = [1,2,3];
let b = agregaElemento(a);

console.log(a);
console.log(b);


// -----------------
// Coercion de tipos
// ------------------
let ca = '2'
let cb = 8

console.log(typeof(ca + cb));  // string + number = string
console.log(typeof(ca * cb));  // string * number = number

