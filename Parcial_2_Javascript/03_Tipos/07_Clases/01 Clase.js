class Empleado {

    constructor(nombre,apellido) {
      this.nombre   = nombre;
      this.apellido = apellido;
    }
  
    obtieneEmpleado() {
      return this.nombre+' '+this.apellido;
    }
}
  
let emp = new Empleado("Juan","Perez");
console.log( emp.obtieneEmpleado() );     // Juan Perez
console.log( typeof emp );                // Object

console.log( typeof Empleado );           // function