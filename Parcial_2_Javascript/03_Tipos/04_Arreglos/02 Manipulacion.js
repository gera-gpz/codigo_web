// La propiedad length es igual al último índice+1
// todos los métodos de array ajustan automáticamente la propiead length.
// ---------------------------------------------------------------------
let vector5=[1,2,3,4,5];
console.log(vector5.length);
console.log(vector5);


// Si reducimos el valor de length, truncaremos el array
// -----------------------------------------------------
let vector6=[1,2,3,4,5];
vector6.length=6;
console.log(vector6.length);
console.log(vector6);



// Convierte a cadena
// ---------------------------------------------------------------------
let vector55=[1,2,3,4,5];
console.log(vector55.toString());
console.log(typeof vector55.toString());

let vector56=[1,2,3,4,5];
console.log(vector56.join(' + '));
console.log(typeof vector56.toString());





// Ordernar e Invertir
// ---------------------------------------------------------------------

let vector57=[5,2,1,3,4];
console.log(vector57.sort())


let vector58=[1,2,3,4,5];
console.log(vector58.reverse())










// Pila, usando los métodos push y pop
// -----------------------------------------------------
let pila = [];

pila.push(1);
pila.push(2);
pila.push(3,4);

let ultimo = pila.pop();
console.log(ultimo);

















// Cola, usando los metodos push y shift
// -------------------------------------------------------
let cola = [];

cola.push(1);
cola.push(2);
cola.push(3,4);

let primero = cola.shift();
console.log(primero);












// Con unshift podemos agregar un elemento al inicio del array
// -----------------------------------------------------------
let vector8 = [];

vector8.push(1);
vector8.push(2);
vector8.push(3,4);

console.log(vector8);
vector8.unshift(0);
console.log(vector8);












// Como los arrays son objetos podemos usar el operador `delete` Para remover elementos
// Pero como delete se usa para remover propiedades de un elemento
// Nos dejará el espacio en el arreglo
// ------------------------------------------------------------------------------------
let arreglo = [1,2,3,4,5,6,7,8,9];

delete arreglo[2];
console.log(String(arreglo));       // [1,2, ,4,5,6,7,8,9]
console.log(arreglo[2]);




// Metodo splice para remover, reemplazar, insertar elementos
// ----------------------------------------------------------
// Remover
let vector9 = [1,2,3,4,5,6];
console.log(vector9);
vector9.splice(1,2);              // Del indice 1 remueve 2 elementos
console.log(vector9);

//Remplazar
let vectorRem = [1,2,3,4,5,6];
console.log(vectorRem);
vectorRem.splice(1,2,200,300);      // Del indice 1 reemplaza 2 elementos
console.log(vectorRem);

//Insertar
let vectorIn = [1,2,3,4,5,6];
console.log(vectorIn);
vectorIn.splice(1,0,200,300);      // Del indice 1 no remueve e inserta 2 elementos
console.log(vectorIn);

//Si el primer parametro es negativo empieza desde el final
let vectorFu=[1,2,3,4,5,6,7];
let vectorBa=vectorFu.splice(-3,2);     // Empezando del final recorre 3 posiciones y toma 2 elementos
console.log(vectorBa);




// Metodo slice para obetener una "rebanada" del arreglo
// -----------------------------------------------------
let vectorm = [1,2,3,4,5,6,7];
let vectorn = vectorm.slice(1,4);   // Regresa un arreglo de la posición 1 a la 4
console.log(vector2);               // Sin tomar la posicion 4

let vectorh=[1,2,3,4,5,6,7];
let vectori=vectorh.slice(-2);     // Ultimos dos elementos
console.log(vectori);




// Método concat para concatenar arreglos
// --------------------------------------
let vectorA=[1,2,3];
let vectorB=[4,5,6];

let vectorC=vectorA.concat(vectorB);
console.log(vectorC);



