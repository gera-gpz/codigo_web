// Recorrer arreglos


// Ciclos clásico for / dowhile / while
//------------
let vectorf=['uno','dos','tres'];
for (let i=0; i<vectorf.length; i++) {
    console.log(vectorf[i]);
}

let vectord=['uno','dos','tres'];
let i = 0;
do {
    console.log(vectord[i]);
    i++;
} while (i<vectord.length)


let vectorw=['uno','dos','tres'];
let j = 0;
while (j<vectorw.length ) {
    console.log(vectorw[j]);
    j++;
}




//Cicl for of
//-----------
let vectorfo=['uno','dos','tres'];
for (let elemento of vectorfo) {
    console.log(elemento);
}

// Los arreglos tienen un metodo forEach()
//----------------------------------------
let vectorfe=['uno','dos','tres'];
vectorfe.forEach(function(elemento, indice, arreglo) { 
    console.log(indice+' = '+elemento+' = '+arreglo); 
} );



// Los arreglos tambien tienen un metodo map()
//---------------------------------------------
let vectorFo = [0,1,2,3,4,5,6,7,8,9];
let vectorBa = vectorFo.map(function(elemento) { return elemento+1; });
console.log(vectorBa);


// map (funcion Flecha)
// --------------------
let vectorFoo = [0,1,2,3,4,5,6,7,8,9];
let vectorBaz = vectorFoo.map( elemento => elemento+1 );
console.log(vectorBaz);