

// La desestructuracion nos permite "sacar" elementos del arreglo y asignarselos a variables.
// La primer variable obtiene el primer elemento la segunda variable el segundo elemento y asi sucesivamente

let arreglo = ["Hola","Juan","Como","amaneciste","hoy","?"];
console.log(arreglo);

// Extrayendo el primer y segundo elemento de la cadena
let [saludo,nombre] = arreglo;
console.log(saludo+" "+nombre);

// Se puede usar la coma como separador para omitir elementos del arreglo
let [elem1,elem2,,elem4,] = arreglo;
console.log(elem1+' '+elem2+' '+elem4);

//Se puede usar el operador spread ()...) para obtener el resto de elementos
let [sal,nom,...pregunta] = arreglo;
console.log(sal+" "+nom+" "+pregunta);



