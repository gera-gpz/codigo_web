
// Declaración de un arreglo con corchetes
// Esta es la forma más usada
// ---------------------------------------
let vector1=[1,2,3,4,5];
console.log(vector1);


// Declaración de un arreglo usando new Array()
// Esto creará un array con la longitud especificada
// pero sin elementos
// -------------------------------------------------
let vector2 = new Array(4);
console.log(vector2);


// Accedemos a los elementos con brackets, empezando de cero
// ---------------------------------------------------------
let vector3=[1,2,3,4,5];
console.log(vector3[3]);





































// Los elementos de un arreglo pueden ser de diferentes tipos
// ----------------------------------------------------------
let vector4=[1,'Juan',true,{nombre:"Juan",edad:20},function(){ console.log("Hola Mundo"); } ];
console.log(vector4[0]);
console.log(vector4[1]);
console.log(vector4[2]);
console.log(vector4[3]);
console.log(vector4[3].nombre);
console.log(vector4[3].edad);
console.log(vector4[4]);
vector4[4]();     //ejecuta la función del arreglo














































// Los arreglos se copian por referencia
// -------------------------------------
let arreglo1 = [1,2,3,4,5];
let arreglo2 = arreglo1

console.log(arreglo1);
console.log(arreglo2);

arreglo2.push(6);

console.log(arreglo1);
console.log(arreglo2);






