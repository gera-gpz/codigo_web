function MostrarFechaHora() {
  	var fecha = new Date();

  	console.log("Hoy es ");
  	console.log(fecha.getDate());
  	console.log(fecha.getMonth());
  	console.log(fecha.getFullYear());
  	console.log("");
  	console.log(fecha.getHours());
  	console.log(fecha.getMinutes());
  	console.log(fecha.getSeconds());
}

MostrarFechaHora();
