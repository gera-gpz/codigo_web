
// Declaracion de cadenas en Javascript
//-------------------------------------
const cad1 = "Esto es una cadena de carácteres";
const cad2 = 'Esto también es una cadena de carácteres';
const cad3 = `Esto también es una cadena de carácteres`;


// Escapando comillas simples dentro de la cadena
//------------------------------------------------
const cad4 = 'Esto es una \'cadena de carácteres';
console.log(cad4);



// Concateando variables con cadenas
//----------------------------------
const nombre1 = "Pepito";
const edad1 = 8;
const cadena1 = "Mi nombre es " + nombre1 + " y tengo " + edad1 + " años";
console.log(cadena1);


//Interpolando variables en una cadena (cadena con backticks)
//----------------------------------------------------------
const nombre2 = `Pepito`;
const edad2 = 8;
const cadena2 =`Mi nombre es ${nombre2} y tengo ${edad2} años, el próximo año tendré ${edad2+1} años`;
console.log(cadena2);



//Metodos de las cadenas
//----------------------
let cadenaEjemplo = "El que madruga Dios lo ayuda";

console.log(cadenaEjemplo.toUpperCase());
console.log(cadenaEjemplo.substring(15,cadenaEjemplo.length));
console.log(cadenaEjemplo.replace("Dios","Diosito"));
