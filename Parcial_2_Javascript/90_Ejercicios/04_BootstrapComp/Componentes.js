    $(document).ready(function() {

      $('#btnAlertaJs').click(function() {
            $('#alerta1').show();
      });

      $('#btnCierraAlerta').click(function() {
            $('#alerta1').hide();
      });

      $('#btnModalData').click(function() {
            console.log("Click en boton 2, el nodal se dispara con atributos de datos de bootstrap");
      });

      $('#btnModalJs3').click(function() {
        $('.modal-body p').text('Se disparo modal desde Boton 3');
        $('#modal1').modal();
      });

      $('#btnModalJs4').click(function() {
        $('.modal-body p').text('Se disparo modal desde Boton 4');
        $('#modal1').modal();
      });

      $('#btnSweetAl').click(function() {
          swal("Sweet Alert", "Presionaste el boton para disparar Sweet Alert", "success");
      });

});