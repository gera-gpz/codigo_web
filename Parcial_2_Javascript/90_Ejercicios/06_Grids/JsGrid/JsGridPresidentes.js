$("document").ready(function () {

  let presidentes = [
    { Nombre: "Andrés Manuel López", Edad: 70, Direccion: "Jalisco 726", EstadoNac: 1, Casado: true, },
    { Nombre: "Enrique Peña Nieto", Edad: 50, Direccion: "Reforma 2007", EstadoNac: 2, Casado: false, },
    { Nombre: "Felipe Calderón", Edad: 65, Direccion: "Guererro 2801", EstadoNac: 1, Casado: false, },
    { Nombre: "Vicente Fox", Edad: 70, Direccion: "Ocampo 825", EstadoNac: 3, Casado: true, },
    { Nombre: "Erneso Zedillo", Edad: 68, Direccion: "Linconln 2138", EstadoNac: 3, Casado: false, },
  ];

  let paises = [
    { Pais: "Tabasco", Id: 0, },
    { Pais: "México", Id: 1, },
    { Pais: "Jalisco", Id: 2, },
    { Pais: "Zacatecas", Id: 3, },
  ];

  $("#jsGrid").jsGrid({
    width: "100%",
    height: "400px",
    inserting: true,
    editing: true,
    sorting: true,
    paging: true,
    data: presidentes,
    fields: [
      { name: "Nombre", type: "text", width: 150, validate: "required", }, 
      { name: "Edad", type: "number", width: 50,},
      { name: "Direccion", type: "text", width: 200, },
      { name: "EstadoNac", type: "select", items: paises, valueField: "Id", textField: "Pais", },
      { name: "Casado", type: "checkbox", title: "Casado", sorting: false, },
      { type: "control", }, 
    ],
  });

});
