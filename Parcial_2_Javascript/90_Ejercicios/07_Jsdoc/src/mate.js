/** @module mate */

/**
 * Esta funcion regresa el area de un cuadrado
 * @param {Number} lado Medida del lado del cuadrado 
 * @returns Number Area del cuadrado
*/
export function areaCuadrado(lado) {
    return lado*lado;
}

/**
 * Esta funcion calcula el atrea de un triangulo
 * @param {Number} base Medida de la base del triangulo 
 * @param {Number} altura Medida de la altura del triangulo 
 * @returns Number Area del Triangulo
*/
export function areaTriangulo(base,altura) {
    return (base*altura)/2;
}

/**
 * Funcion para calcular el area de un circulo
 * @param {Number} radio Medida del radio del circulo 
 * @returns Number
 */
export function areaCirculo(radio) {
    return Math.pow((Math.PI*radio),2);
}

