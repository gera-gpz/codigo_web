    $(document).ready(function() {

      // En el último parametro del método open
      // indicamos si la petición será: 
      // asincrona = true  = La página continua su ejecución
      // asincrona  = false = La página queda bloqueada esperando la respuesta
      //
      // En el archivo getHora.php se simula un retardo
      // con el metodo sleep

      $('#btnHoraAsincrono').click(function() {
            var solicitud = new XMLHttpRequest();
            solicitud.onload = function() {
                  document.getElementById("hora").value=solicitud.responseText;
            }
            solicitud.open("GET","getHora.php",true);
            solicitud.send();
      });

      $('#btnHoraSincrono').click(function() {
            var solicitud = new XMLHttpRequest();
            solicitud.onload = function() {
                  document.getElementById("hora").value=solicitud.responseText;
            }
            solicitud.open("GET","getHora.php",false);
            solicitud.send();
      });

      $('#btnLimpiaHora').click(function() {
            document.getElementById("hora").value="";
      });

});