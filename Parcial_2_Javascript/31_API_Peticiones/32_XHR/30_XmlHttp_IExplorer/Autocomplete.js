agregarEvento(window,'load',iniciarEventos,false);


function iniciarEventos()
    {    agregarEvento(document.getElementById("palabra"),'keyup',presionTecla,false); 
    }



var objetoAjax;
function presionTecla(e)
    {
    objetoAjax = crearObjetoAjax();
    objetoAjax.onreadystatechange = obtenerDatos;
    var palabra = document.getElementById('palabra').value;
    objetoAjax.open('GET','Ejemplo_Js_Autocomplete.php?palabra='+palabra,true);
    objetoAjax.send(null);
    }


function obtenerDatos()
    {
    var resultados = document.getElementById("resultados");
    
    if (objetoAjax.readyState==4)
        {
        if (objetoAjax.status==200)
            {
            var xml=objetoAjax.responseXML;
            var pals = xml.getElementsByTagName('palabra');
            resultados.innerHTML='';
            for (i=0; i<pals.length; i++)
                 { 
                 resultados.innerHTML = resultados.innerHTML+pals[i].firstChild.nodeValue+'<br>';
                 }
            }
        else {
                alert(objetoAjax.statusText);
              }
        }
    else
        {
            resultados.innerHTML='Cargando....';
        }
    }




function agregarEvento(objeto,evento,funcion,captura)
    {
    if (objeto.atachEvent)
         {   objeto.atachEvent('on'+evento,funcion);
            return true;
         }
    else if (objeto.addEventListener)
             { objeto.addEventListener(evento,funcion,captura);
               return true;
             }
         else return false;
    }


function crearObjetoAjax()
    {
    var ajaxRequest=null;
    
    if (window.ActivexObject)
        ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
    else
        if (window.XMLHttpRequest)
            ajaxRequest = new XMLHttpRequest();
    
    return ajaxRequest;
    }