function showCliente(str) {
    if (str == "0") {
        document.getElementById("ClienteDescripcion").innerHTML = "SELECCIONE";
        return;
    }

    var xmlhttp;

    if (window.XMLHttpRequest) { xmlhttp = new XMLHttpRequest(); } else { xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); }

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) { document.getElementById("ClienteDescripcion").innerHTML = xmlhttp.responseText; }
    };

    xmlhttp.open("GET", "Ejemplo_db.php?q=" + str, true);
    xmlhttp.send();
}
