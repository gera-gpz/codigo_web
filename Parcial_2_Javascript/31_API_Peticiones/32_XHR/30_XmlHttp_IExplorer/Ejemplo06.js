
addEvent(window,'load',inicializarEventos,false);


function inicializarEventos()
{
    addEvent(document.getElementById("boton1"),'click',presionBoton,false);
}

function presionBoton()
{
    var objeto1=document.getElementById("seleccion");   
    var objeto2=document.getElementById("nombre");
    cargarVoto(objeto1.value,objeto2.value);
}

var conexion1;
function cargarVoto(voto,nombre)
{
    conexion1= ajaxRequest();
    conexion1.onreadystatechange = procesarEventos;
    conexion1.open('GET','Ejemplo06.php?parVoto='+voto+'&parNombre='+nombre,true);
    conexion1.send(null);
}

function procesarEventos()
{
    var resultados = document.getElementById("resultados");
    if (conexion1.readyState == 4)
        { resultados.innerHTML = conexion1.responseText; }
    else
        { resultados.innerHTML = 'Cargando...'; }

}




/* ---------- */
/*  Funciones */
/* ---------- */

function addEvent(objeto,evento,funcion,captura)
{
    if (objeto.atachEvent)
        { 
        objeto.atachEvent('on'+evento,funcion);
        return true;
        }
    else 
        if (objeto.addEventListener)
             {
              objeto.addEventListener(evento,funcion,captura);
              return true;
             }
        else  return false;
}

function ajaxRequest()
{
    var xmlHttp=null;
    
    if (window.ActiveXObject)
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
    else
        if (window.XMLHttpRequest)
        xmlHttp = new XMLHttpRequest();
    
    return xmlHttp;
}






