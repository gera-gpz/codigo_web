agregarEvento(window, 'load', iniciarEventos, false);

function iniciarEventos() {
    agregarEvento(document.getElementById("carreras"), 'change', obtieneMaterias, false);
}

var conexionAjax;


function obtieneMaterias(e) {
    var codigo = document.getElementById("carreras").value;
    if (codigo !== 0) {
        conexionAjax = crearObjetoAjax();
        conexionAjax.onreadystatechange = Proceso;
        conexionAjax.open("GET", "cargarSelect.php?codigo=" + codigo, true);
        conexionAjax.send(null);
    } else {
        var select2 = document.getElementById("materias");
        select2.options.length = 0;
    }
}


function Proceso() {
    if (conexionAjax.readyState == 4) {
        document.getElementById("espera").innerHTML = "";

        var xml = conexionAjax.responseXML;
        var pals = xml.getElementsByTagName('materia');
        var select2 = document.getElementById('materias');
        select2.options.length = 0;
        for (f = 0; f < pals.length; f++) {
            var op = document.createElement("option");
            var texto = document.createTextNode(pals[f].firstChild.nodeValue);
            op.appendChild(texto);
            select2.appendChild(op);
        }
    } else {
        document.getElementById("espera").innerHTML = "Cargando...";
    }
}



//*********************************
// Funciones Comunes
//*********************************

function agregarEvento(objeto, evento, funcion, captura) {
    if (objeto.atachEvent) {
        objeto.atachEvent('on' + evento, funcion);
        return true;
    } else if (objeto.addEventListener) {
        objeto.addEventListener(evento, funcion, captura);
        return true;
    } else return false;
}

function crearObjetoAjax() {
    var ajaxReq = null;

    if (window.ActiveXObject)
        xmlHttp = newActiveXObject("Microsoft.XMLHTTP");
    else
    if (window.XMLHttpRequest)
        ajaxReq = new XMLHttpRequest();
    else
        alert("Error creaccion Objeto Ajax");

    return ajaxReq;
}
