agregarEvento(window, 'load', iniciarEventos, false);


function iniciarEventos() {
    agregarEvento(document.getElementById("boton"), 'click', presionBoton, false);
}


function presionBoton(e) {
    var obj = document.getElementById('zona');
    recuperaDatos(obj.value);
}

var objetoAjax;

function recuperaDatos(valor) {
    objetoAjax = crearObjetoAjax();
    objetoAjax.onreadystatechange = Proceso;
    objetoAjax.open('GET', 'Ejemplo_Json.php?parametro=' + valor, true);
    objetoAjax.send(null);
}

function Proceso() {
    var resultados = document.getElementById("resultados");
    if (objetoAjax.readyState == 4) {
        var datos = eval('(' + objetoAjax.responseText + ')');

        var salida = "Nombre : " + datos.nombre + "<br>";
        salida = salida + "Apellido : " + datos.apellido + "<br>";
        salida = salida + "Telefono : " + datos.telefono + "<br>";
        resultados.innerHTML = salida;
    } else { resultados.innerHTML = "Cargando..."; }
}




function agregarEvento(objeto, evento, funcion, captura) {
    if (objeto.atachEvent) {
        objeto.atachEvent('on' + evento, funcion);
        return true;
    } else if (objeto.addEventListener) {
        objeto.addEventListener(evento, funcion, captura);
        return true;
    } else return false;
}


function crearObjetoAjax() {
    var ajaxRequest = null;

    if (window.ActivexObject)
        ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
    else
    if (window.XMLHttpRequest)
        ajaxRequest = new XMLHttpRequest();

    return ajaxRequest;
}
