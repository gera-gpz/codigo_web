
agregarEvento(window,'load',asignarEventos,false);

function asignarEventos()
    {
    agregarEvento(document.getElementById('boton'),'click',obtieneData,false);
    }

var objetoAjax;
function obtieneData(e)
    {
    objetoAjax=crearObjetoAjax();
    objetoAjax.onreadystatechange = ConsultaData;
    objetoAjax.open('GET','Ejemplo_db_json.php',true);
    objetoAjax.send(null)
    }

function ConsultaData()
{
    var resultados = document.getElementById('resultados');
    resultados.innerHTML='';
    
    if ( objetoAjax.readyState == 4 )
        {
        alert('Respuesta en formato JSON : '+objetoAjax.responseText);

        var datos=eval(objetoAjax.responseText);
        /*var datos=JSON.parse(objetoAjax.responseText);*/

        var salida="";

        alert(datos.id_cliente);
            
        for(i=0; i<datos.length; i++)
            {
            salida += 'Id : '+ datos.id_cliente+'<br>';
            salida += 'Nombre : '+datos.nombre+'<br>';
            salida += 'Direccion : '+datos.direccion+'<br>';
            salida += 'Telefono : '+datos.telefono+'<br>';
            }
         resultados.innerHTML=salida;
        }
    else
        { resultados.innerHTML='Cargando...'; }
        
}


function agregarEvento(objeto,evento,funcion,captura)
    {
    if (objeto.atachEvent)
         {   objeto.atachEvent('on'+evento,funcion);
            return true;
         }
    else if (objeto.addEventListener)
             { objeto.addEventListener(evento,funcion,captura);
               return true;
             }
         else return false;
    }


function crearObjetoAjax()
    {
    var ajaxRequest=null;
    
    if (window.ActivexObject)
        ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
    else
        if (window.XMLHttpRequest)
            ajaxRequest = new XMLHttpRequest();
    
    return ajaxRequest;
    }