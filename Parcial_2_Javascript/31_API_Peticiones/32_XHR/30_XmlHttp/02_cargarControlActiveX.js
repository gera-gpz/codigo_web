function ajaxFunction() {
    //CREATE XMLHttpRequest OBJECT TO COMUNICATE WITH THE SERVER
    //-----------------------------------------------------------
    var ajaxRequest;
    try { // Firefox Chrome Safar Opera
        ajaxRequest = new XMLHttpRequest();
    } catch (e) {
        try { // Internet Explorer Browsers
            ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) { // Something went wrong
                alert("Your browser broke!");
                return false;
            }
        }
    }

    //FUNCTION TO PROCESS THE RESPONSE FROM THE SERVER
    //-------------------------------------------------
    ajaxRequest.onreadystatechange = function() {
        if (ajaxRequest.readyState == 4) //Is the response complete?
        { document.formulario1.hora.value = ajaxRequest.responseText; } //Retrieve server reponse
    };

    ajaxRequest.open("GET", "cargarControlText.php", true); //Set  Request
    ajaxRequest.send(); //Send Request
}
