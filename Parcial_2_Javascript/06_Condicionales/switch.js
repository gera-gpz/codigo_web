// -----------------
// Ejemplo de SWITCH
// -----------------

var numero;
var color;

numero = prompt("Teclee un valor del 1 al 5 : ", "");
numero = parseInt(numero);

switch (numero) {
  case 1:
    document.write("Tecleaste Uno");
    break;
  case 2:
    document.write("Tecleaste Dos");
    break;
  case 3:
    document.write("Tecleaste Tres");
    break;
  case 4:
    document.write("Tecleaste Cuatro");
    break;
  case 5:
    document.write("Tecleaste Cinco");
    break;
  default:
    document.write("Valor Invalido");
    break;
}

console.log("Ejemplo 2 de Switch");

color = prompt("Teclee el color para el fondo (rojo,verde,azul) : ", "");
switch (color) {
  case "rojo":
    document.bgColor = "#ff0000";
    break;
  case "verde":
    document.bgColor = "#00ff00";
    break;
  case "azul":
    document.bgColor = "#0000ff";
    break;
}
