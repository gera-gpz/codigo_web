// ------------------------------------------
// Ciclos clasicos en Javascript
// ------------------------------------------

for (let x=1; x<=5; x++) {                       // ciclo for
        console.log('x = ' + x);
}


let y = 1;
while ( y <= 5) {                               // ciclo while
    console.log('y = ', y);
    y++;
}


var z = 0;
do {                                            // cliclo do-while"
    console.log('z = ' + z);    
    z++;
} while (z <= 5);



// ------------------------------------------
// Ciclos for-in (Propiedades de objetos)
// ------------------------------------------

let objeto = {
    nombre:"Gerardo",
    apellidop:"Pineda",
    apellidom:"Zapata",
} 

for (let propiedad in objeto) {
    console.log(propiedad + ' = ' + objeto[propiedad]);  // Accedemos al nombre de la propiedad 
}                                                        // Y al valor de la propiedad con corchetes






// ------------------------------------------
// Ciclos for-of (Elementos de objetos Iterables)
// Cadenas, Arreglos, NodeList, HTML Collection
// Nos da acceso al elemento de la coleccion
// ------------------------------------------

let arreglo= [1,2,3,4,5];
let cadena = "Popocateptl";
let object = { nombre:"Gerardo",
               apellido:"Pineda",
               departamento:"sistemas",
             } 

for (let elemento of arreglo) {
      console.log(elemento);           // Tendremos acceso al elemento del arreglo
}

for (let letra of cadena) {
    console.log(letra);                // Tenderemos a los caracteres de la cadena
}

for (let elem of object) {
    console.log(elem + ' = ' + object[elem]);  // TYPE ERROR: No podemos iterar sobre un objeto
}                                              //             Los objetos no son iterables

