

let vector = [];
vector.push('1','2');
vector.push('3','4');
vector.unshift('5');
vector.push('6');
console.log(vector);


// [ 1, 2, 3, 4, 6, 5 ]
// [ 6, 1, 2, 3, 4, 5,]
// [ 5, 1, 2, 3, 4, 6 ]      OK




// ------------------------------------------------------------------------------------
// La segunda segunda declaracion con var es una reasignacion
// ------------------------------------------------------------------------------------

var version = '1';
console.log(version);

if (true) {
    version = '2';
    console.log(version);
}

console.log(version);

// 1   2   1
// 1   2   2    OK





// ------------------------------------------------------------------------------------------------
// Las variables declaradas con let son visibles solo en el bloque de codigo donde esten declaradas
// ------------------------------------------------------------------------------------------------

function Hola() {
    let saludo = "Hola";
}
Hola();
console.log(saludo);


// Reference Error   OK
// undefined
// Saludo





// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------
//  Hoisting.
// -------------------------------------------------------------------------------

function saludar() {
    frase='Hola';
    var frase;
    console.log(frase);
}
saludar();


// Reference Error
// undefined         OK
// Saludo



