// Async/Await
//
// async
// La palabra async declara una función como asíncrona ("va a tardar")
// Al declararla como función asíncrona va a devolver una promesa
//
// await
// La palabra clave await simplemente espera a que se resuelva la promesa 
// aunque nuestri programa javascript sigue corriendo


// Declaracion de función y su ejecución
// -------------------------------------
function cuadradoFoo(a) {
    return a*a;
}

let valorFoo = cuadradoFoo(2)
console.log(valorFoo);







// Si declaramos que la función es asincrona, nos devolverá una promesa
// --------------------------------------------------------------------
async function cuadradoBar(a) {
    return a*a;
}

let valor = cuadradoBar(2)
console.log(valor);








// Las promesas las consumimos con su método then()
//-------------------------------------------------
async function cuadradoBaz(a) {
    return a*a;
}

cuadradoBaz(2).then( (valor)=>{console.log(valor)} );
//Resumiendo
cuadradoBaz(2).then( valor=>console.log(valor) );
//Resumiendo mas :S
cuadradoBaz(2).then( console.log )






// await se usa dentro de una funcion async para "esperar" a que la promesa se cumpla
//-----------------------------------------------------------------------------------
async function cuadradoFooBar(a) {

    let promesa = new Promise(function (resolve, reject) {
        console.log("Inicia Promesa")
        setTimeout(() => resolve(a*a), 5000)
    });
  
    let resultado = await promesa; // Esperar hasta que la promesa se resuelva (PAUSA)
    console.log("Termina Promesa");
    console.log(resultado);        // Promesa resuelta se ejecuta esta instruccion
}

console.log("Inicia Programa");
cuadradoFooBar(2);
console.log("Termina Programa");
