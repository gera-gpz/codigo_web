

/*
   functionX symbols are references to individual functions
   parallel([function1, function2, function3, function4], data => {
         //do something with the combined output once they all finished
   })
*/

function parallel(funcs, callback) {
    var results         = []
    var callsToCallback = 0
    
    funcs.forEach( fn => {          // iterate over all functions
        setTimeout(fn(done), 200)   // and call them with a 200 ms delay
    })
    
    function done(data) { // the functions will call this one when they
                          // finish and they’ll pass the results here
        results.push(data)
        if(++callsToCallback == funcs.length) {
        callback(results)
        }
    }
}