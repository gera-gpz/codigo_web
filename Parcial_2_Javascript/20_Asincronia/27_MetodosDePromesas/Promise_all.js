
function promesa1() {
    return new Promise(( resolve,reject )=>{  
        setTimeout( ()=>{resolve("Prom1 resuelta")},5000  );
     })
}

function promesa2() {
    return new Promise(( resolve,reject )=>{  
        setTimeout( ()=>{resolve("Prom2 resuelta")},1000  );
     })
}

function promesa3() {
    return new Promise(( resolve,reject )=>{  
        setTimeout( ()=>{reject("Prom3 rejected")},1000  );
     })
}

// promesa1().then( (result)=>{ console.log(result); } )
// promesa2().then( (result)=>{ console.log(result); } )


Promise.all([promesa1(),promesa2(),promesa3()])
    .then(( resultado)=>{ console.log(resultado) })
    .catch(( error)=>{ console.log(error) })

