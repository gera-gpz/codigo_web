
function promesa1() {
    return new Promise( (resolve,reject)=>{     
        setTimeout(  ()=>{  resolve( "Promesa1 Aceptada") }   , 5000 )
    }  )
}

function promesa2() {
    return new Promise( (resolve,reject)=>{     
        setTimeout(  ()=>{  reject( "Promesa2 Rechazada") }   , 1000 )
    }  )
}

function promesa3() {
    return new Promise( (resolve,reject)=>{     
        setTimeout(  ()=>{  resolve( "Promesa3 Aceptada") }   , 3000 )
    }  )
}

// promesa1().then( (result)=>{ console.log(result); } )
// promesa2().then( (result)=>{ console.log(result); } )


Promise.allSettled([promesa1(),promesa2(),promesa3()])
    .then( (resultado) => { console.log(resultado) })