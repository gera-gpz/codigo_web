
const promesaOk = (tiempo) => {                   // Promesa que es resuelta despues de cierto tiempo
  return new Promise( (resolve, reject) => {
        setTimeout(() => { resolve(`Promesa resuelta en ${tiempo}`)}, tiempo)
  })
}

const promesaErr = (tiempo) => {                  // Promesa que es rechazada despues de cierto tiempo
    return new Promise(  (resolve,reject) =>{   
        setTimeout( ()=>{reject(`Promesa Fechazada en ${tiempo}`)} , tiempo   )
    })
}
  
  
// Consumiendo una promesa
promesaOk(1000)
   .then(result => console.log(result))
   .catch(error => console.log(error))
  
// promesaErr(1500)
//    .then(result => console.log(result))
//    .catch(error => console.log(error))

// Consumiendo n promesas
// Promise.all([promesaOk(1000), promesaErr(3000), promesaOk(1500)])
//    .then(result => console.log(result))   //Promesas se completaron con exito
//    .catch(error => console.log(error))    //Una promesa fue rechazada