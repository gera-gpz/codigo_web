
// Suponiendo que tenemos una función que recibe dos parametros y calcula un sueldo
// La secuencia de pasos seria la siguiente:

function desplegarSueldo(sueldo) {
    console.log(sueldo);
}

function calcularSueldo(parBase,parHoras) {
    let calculo = parBase * parHoras;
    return calculo;
}


let sueldo = calcularSueldo(100,40);
desplegarSueldo(sueldo);
