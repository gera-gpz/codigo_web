
// La función setTimeOut() nos permite llamar a una función (en el primer parametro)
// después de los milisegundos que le especifiquemos (en su segundo parametro)

setTimeout(console.log("Hola Mundo"), 3000);





// Callback como declaración de función
// -----------------------------------------------------------------------------------------
function imprimeMensaje1() {      //Declaramos una función
    console.log('Hola Mundo');
}

setTimeout(imprimeMensaje1,3000);  //Se la pasamos a la función SetTimeOut() que la recibe
                                   //y la ejecutará solo hasta que pasen 3 segundos
                                   //imprimeMensaje1 es una funcion callback





// Callback como expresión de función
// -----------------------------------------------------------------------------------------
const imprimeMensaje2 = function() {  //Asignamos una función a auna constante
    console.log('Hola Mundo');
}

setTimeout(imprimeMensaje2,3000);  //Se la pasamos a la función SetTimeOut() que la recibe
                                   //y la ejecutar± solo hasta que pasen 3 segundos






// Callback pasado como función anónima
// -----------------------------------------------------------------------------------------
setTimeout(function() { console.log('Hello World'); },5000);








// Callback pasado como función flecha
// -----------------------------------------------------------------------------------------
setTimeout( ()=> { console.log('Hello World') } ,5000);







console.log("Inicia Programa")
setTimeout( ()=> { console.log('Hello World') } ,5000);
console.log("Termina Programa")



