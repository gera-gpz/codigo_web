
// O bien podemos pasar la función desplegarsueldo()
// como parámetro calback a la función calcularsueldo()
// que la usará cuando pase o suceda algo

function desplegarSueldo(sueldo) {
    console.log(sueldo);
}

function calcularSueldo(parBase,parHoras,funDespliega) {
    let calculo = parBase * parHoras;
    funDespliega(calculo);
}

calcularSueldo(250,40,desplegarSueldo);
