// ----------
// Callback 1
// ----------

function calculaCalificacion(parQuiz,parExam,parTarea,fnCallback) {
     let cal=(parQuiz*.10)+(parExam*.60)+(parTarea*.30);
     if (cal >= 70) {
         fnCallback(false,"Aprobado");
     } else {
         fnCallback(true,"Reprobado");
     }
}

calculaCalificacion(20,70,90, function(error,resultado) {
    if (error) {
      console.log(resultado);
    } else {
      console.log(resultado);
    }
});







// ----------
// Callback 2
// ----------

function calculaCalificacion(parQuiz,parExam,parTarea,fnCallback) {
  let cal=(parQuiz*.10)+(parExam*.60)+(parTarea*.30);
  if (cal >= 70) {
      fnCallback(false,"Aprobado");
  } else {
      fnCallback(true,"Reprobado");
    }
}

calculaCalificacion(20,70,90, (error,resultado) => {
    if (error) {
      console.log(resultado);
    } else {
      console.log(resultado);
    }
});


// ----------
// Callback 3
// ----------

function calculaCalificacion(parQuiz,parExam,parTarea,fnCallback) {
    let cal=(parQuiz*.10)+(parExam*.60)+(parTarea*.30);
     if (cal >= 70) {
         fnCallback(false,"Aprobado");
     } else {
         fnCallback(true,"Reprobado");
       }
}

calculaCalificacion(20,70,90, imprime);

function imprime(error,resultado) {
       if (error) {
         console.log(resultado);
       } else {
         console.log(resultado);
       }
}
