
// Podríamos llamar la función desplegaSueldo()
// desde dentro de la función calcularSueldo()

function desplegarSueldo(sueldo) {
    console.log(sueldo);
}

function calcularSueldo(parBase,parHoras) {
    let calculo = parBase * parHoras;
    desplegarSueldo(calculo);
}

calcularSueldo(100,40);
