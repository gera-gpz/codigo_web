ó// ----------------------
// Declaración de Función (El RETURN DEBE ESTAR EXPLICITO)
function cuadrado(a) { 
    return a*a; 
}
console.log( cuadrado(2) );


// Función Flecha
// ----------------------
let cuadradof = (a) => { 
    return a*a; 
}
console.log( cuadradof(2) );


// Funcion Flecha con Return Implicito
// ----------------------
let cuadradoofaNmber = a => a*a;
console.log( cuadradoofaNmber(2) );


