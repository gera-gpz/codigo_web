//-----------------------------------------
//Creación de la Promesa (Código Productor)
//-----------------------------------------
let promesa = new Promise(function(resolve,reject) {               //Esta función se ejecuta al instanciar

    console.log('Ejecutando promesa.....');
    if (false) {
    setTimeout( ()=>resolve({mensaje:"Promesa Aceptada"}), 5000);  //Simulamos un retardo e indicamos 
                                                                   //que la promesa fue aceptada
    } else {
    setTimeout( ()=>reject(new Error("Promesa Rechazada")), 5000); //Simulamos un retardo e indicamos
                                                                   //que la promesa fue rechazada
    }                                    
});

//-----------------------------------------
//Consumo de la Promesa (Código Consumidor)
//-----------------------------------------
// El objeto promesa tiene los metodos .then() .catch()
// Para manejar la aceptación y el rechazo de la promesa
console.log("Inicia Programa Js");
promesa.then(function(res)  { console.log(`Exito ${res.mensaje}`) })
       .catch(function(err) { console.log(`Error ${err.message}`) });
console.log("Termina Programa Js");





//-----------------------------------------
//Consumo de la Promesa (Código Consumidor)
//-----------------------------------------
// El objeto promesa tiene el metodo .finally()
// que se ejecutará tanto en la aceptacion como en el rechazo
console.log("Inicia Programa Js");
promesa.then(function(res)  { console.log(`Exito ${res.mensaje}`) })
       .catch(function(err) { console.log(`Error ${err.message}`) })
       .finally(function()  { console.log(`Este codigo siempre se ejecuta`) });
console.log("Termina Programa Js");




//-----------------------------------------
//Consumo de la Promesa (Código Consumidor)
//-----------------------------------------
// El objeto promesa puede manejar la aceptación y rechazo 
// en el método .then() 
// console.log("Inicia Programa Js");
promesa.then(
  function(result) { console.log(`Exito ${result.mensaje}`) },
  function(error)  { console.log(`Error ${error.message}`) }
);
console.log("Termina Programa Js");




//-----------------------------------------
//Consumo de la Promesa (Código Consumidor)
//-----------------------------------------
// Manejando la aceptación y rechazo 
// en el método .then() y ejecutando codigo siempre con finally()
console.log("Inicia Programa Js");
promesa.then( function(result) { console.log(`Exito ${result.mensaje}`) },
              function(error)  { console.log(`Error ${error.message}`) })
       .finally(function() { console.log(`Este codigo siempre se ejecuta`) });
console.log("Termina Programa Js");































//-----------------------------------------
// Consumo de la Promesa (Código Consumidor)
// Simplificando
//-----------------------------------------

// Podemos usar funciones flecha
//
promesa.then(
    (result) => { console.log(result) },
    (error)  => { console.log(error.message)  }
  );


// Si la funcion flecha recibe solo 1 paramero y tiene solo 1 instruccion
// parentesis y corchetes de la funcion son opcionales:
//
  promesa.then(
  result => console.log(result),
  error  => console.log(error.message)
);

