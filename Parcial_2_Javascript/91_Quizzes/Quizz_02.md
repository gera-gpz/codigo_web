 <!-- Segundo Quizz del Tercer Parcial
      Conceptos de Javascript básicos -->

# Quizz 2 de Javascript


1) En Javascript los tipos de datos primitivos se comportan  
como objetos con propiedades y métodos.  

> - **Verdadero**
> - Falso

2) El ámbito de las variables declaradas con esta palabra  
   clave sera de bloque:  

> - **let**
> - var
> - set

3) La siguiente declaración de variable **NO ES** válida en Javascript:   

> - let moneda="USD";
> - let $=1;
> - **let peso_total=1000;**
> - let sueldo-base=2000;


4) Término usado en Javascript para refierse al alcance de una variable:  

> - Hoisting
> - **Scope**
> - Bubbling
> - Event Propagation

5) A las variables declaradas con let y const NO se les hace hoisting.  

> - **Verdadero**
> - Falso


6) En Javascript si no le asignamos valor a una variable su valor será:  

> - null
> - **undefined**
> - Infinite
> - NaN

7) El hoisting se le hace a la declaración y asignación de una variable.  

> - Verdadero
> - **Falso**


8) Cuál de lo siguientes tipos NO es un primitivo:  

> - number
> - undefined
> - **array**
> - string

9) Cuando declaramos variables globales con var, se agregan como propiedades   
   del objeto global window y con let no sucede esto.  

> - **Verdadero**
> - Falso

10 Con var, es permitido declarar una variable mas de una vez.
> - **Verdadero**
> - Falso