
// -------------------------------
// Variables declaradas con var se pueden volver a declarar
// -------------------------------
var version = '1';
var version = '2';
console.log(version);

if (true) {
  var version = '2';
  console.log(version);
}
console.log(version);




// -------------------------------
// Dentro de la función estaremos accesando la global
// -------------------------------
var version = '1';
console.log(version);

function miFuncion() {
  version = '2';
  console.log(version);
}
miFuncion();
console.log(version);

// 1  2  2  [OK]
// 1  2  1
// 1  1  2




// -------------------------------
// Variable Global
// ------------------------------
function miFuncion() {
  let x = 100;
  if (true) {
    let x = 200;
    console.log(x);   // 200
  }
  console.log(x);     // 100
}




// -------------------------------
// Ejemplo de alcances
// ------------------------------
var a = 5;
var b = 10;

if (a === 5) {
  let a = 4; // El alcance es dentro del bloque if
  var b = 1; // El alcance es dentro de la función

  console.log(a);  // 4
  console.log(b);  // 1
}

console.log(a); // 5
console.log(b); // 1



//-----------------------------------
// 11 Insertar
//-----------------------------------
let vector=[1,2,3,4,5];
vector.forEach(function(valor, indice, arreglo) { 
    arreglo[indice]=valor+2;
} );

console.log(vector);

// [1, 2, 3, 4, 5]
// [3, 4, 5, 6, 7]    //ok

