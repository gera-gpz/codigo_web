// Tercer Quizz del Tercer Parcial
// Métodos de la clase array


// 1) Modificación de la propiedad length el array
//-------------------------------------------------------

let vector=[1,2,3,4,5];
vector.length=3;
console.log(vector);

// [1, 2, 3, 4, 5]
// [0, 1, 2]
// [1, 2, 3]        //OK


// 2) Pila y Colas 
//--------------------------------------------------------

let vector = [];
vector.push(2,4);
vector.push(3);
vector.pop();
vector.push(5,8);
console.log(vector);

// [2, 4, 5, 8]   //OK
// [2, 4, 3, 5]
// [2, 4, 3, 8]



// 3) Pilas y Colas
//--------------------------------------------------------

let vector = [];
vector.push(2,4);
vector.push(3,5);
vector.shift(1);
vector.push(8,9);
console.log(vector);

// [ 2, 4, 3, 5, 1 ]
// [ 4, 3, 5, 8, 9 ]  //OK
// [ 2, 4, 3, 8, 9 ]


// 4) Usar Delete en un array
//----------------------------------------------------------

let vector = [1,2,3,4,5];
delete vector[2];
console.log(vector);

// [1,  2,  ,  4,  5]  //ok
// [1,  ,  3,  4,  5]
// [ ,  ,  3,  4,  5]



// 5) Usar unshift en un array
//----------------------------------------------------------

let vector = [];
vector.push(2,4);
vector.push(3,5);
vector.unshift(0);
vector.push(8,9);
console.log(vector);

// [ 2, 4, 3, 0, 5, 9, 0 ]
// [ 0, 2, 4, 3, 5, 8, 9 ]  //ok
// [ 2, 4, 3, 5, 8, 9, 0 ]



// 6) Splice remover
//----------------------------------------------------------

let vector = [0,1,2,3,4,5,6];
vector.splice(3,2);
console.log(vector);

// [ 0, 3, 4, 5, 6 ]
// [ 0, 1, 2, 5, 6 ]  //ok
// [ 1, 2, 2, 4, 5 ]


//7) Concatenar
//----------------------------------------------------------

let vector1=[1,2,3]; vector1.push(2);
let vector2=[4,5,6]; vector2.shift(1);

let vector3=vector1.concat(vector2);
console.log(vector3);


// [ 2, 1, 2, 3, 5, 6 ]
// [ 3, 4, 5, 6, 1, 1 ]
// [ 1, 2, 3, 2, 5, 6 ]   //ok



// 8 Insertar
//----------------------------------------------------------

let vector = [1,2,3,4,5,6];
vector.splice(1,0,200,300);
console.log(vector);

// [ 200, 300, 1, 2, 3, 4, 5, 6 ]
// [ 1, 200, 300, 2, 3, 4, 5, 6 ]  //ok
// [ 1, 200, 300, 4, 5, 6 ]


// 9 Insertar
//----------------------------------------------------------

let vector1 = [4,8,1,2,4];
let vector2 = vector1.slice(1, 4);
vector2.unshift(5,0);
console.log(vector2);

// [ 4, 8, 1, 2, 5 ]
// [ 5, 0, 4, 8, 1 ]
// [ 5, 0, 8, 1, 2 ]    //ok


// 10 Función dentro de un arreglo
//----------------------------------------------------------

let vector1=[1,true,function(){ console.log("Hola Mundo"); } ];
vector1[2]();


// Function
// Hola Mundo                      //ok
// console.log("Hola Mundo");
