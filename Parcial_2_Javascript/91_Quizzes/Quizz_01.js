// Primer Quizz del Tercer Parcial
// Declaración y alcance de variables

//--------------------------
// 1) variables declaradas con let tienen ambito de bloque
//--------------------------
if (true) {
  let saludo = "Hola";
}
console.log(saludo);

// [1] Reference Error
//  2  Undefined
//  3  Hola




//--------------------------
// 2) A una variable declarada con var "no le importan" los bloques
//--------------------------
if (true) {
    var saludo = "Hola";
}
console.log(saludo);

//  1  Reference Error
//  2  Undefined
// [3] Hola




// -------------------------------
// 3) Variable declarada con let tiene ambito de bloque
//    En este casi el bloque es la función en sí
// -------------------------------
function Hola() {
    let saludo = "Hola";
}
Hola();
console.log(saludo);

// [1] Reference Error
//  2  Undefined
//  3  Hola




// -------------------------------
// 4) La variable declarada con var tiene ambito de bloque
// -------------------------------
function Hola() {
    var saludo = "Hola";
}
Hola();
console.log(saludo);

// [1] Reference Error
//  2  Undefined
//  3  Hola




// -------------------------------
// 5) Al omitir var de la declaración, la variable pasará a ser global
// -------------------------------
function Hola() {
   saludo = "Hola";
}
Hola();
console.log(saludo);

//  1  Reference Error
//  2  Undefined
// [3] Hola




// -------------------------------
// 6) Ejemplo de Hoisting
// -------------------------------
function saludar() {
    frase = "Hola";
    console.log(frase);
    var frase;
}
saludar();

//  1  Reference Error
//  2  Undefined
// [3] Hola




// -------------------------------
// 7) El Hoisting se hace solo a la declaración
//    No a la asignación
// -------------------------------
function saludar() {
    console.log(frase);
    var frase="Hola";
}
saludar();

//  1   Reference Error
// [2]  Undefined
//  3   Hola




// -------------------------------
// 8) Incluso si una variable está dentro de un bloque
//    de código, la variable será elevada
// -------------------------------
function saludar() {
    frase = "Hola";
    console.log(frase);
    if (false) {
        var frase;
    }
}
saludar();

//  1    Reference Error
//  2    Undefined
// [3]   Hola




// -------------------------------
// 9 Ambito de variables con let
// -------------------------------
let version = '1';            // Esta es una variable global
console.log(version);

if (true) {
  let version = '2';          // Esta es una variable de bloque
  console.log(version);
}

console.log(version);         // Aqui accesamos a la variable global
console.log(typeof version);  // Operados tupeof para saber el tipo

// [1]   1  2  1
//  2    1  2  2
//  3    1  2  Undefined





// -------------------------------
// 10 Ambito de variables con var
// -------------------------------
var version = '1';
console.log(version);

function miFuncion() {
  var version = '2';
  console.log(version);
}

miFuncion();
console.log(version);

// [1]   1  2  1
//  2    1  2  2
//  3    1  2  Undefined
