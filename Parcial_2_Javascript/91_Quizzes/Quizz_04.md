# Examen de Nivelación Tercer Parcial


- 1 Cuál es el ámbito de variables declaradas con var
   y cuál el ámbito de las declaradas con let?


- 2 Qué es el hoisting an Javascript?


- 3 Qué significa que un lenguaje sea de tipado dinámico como Javascript
   y cuáles son los tipos de datos de Javascript?


- 4 Cuál es la salida del siguiente código?

````js
function Hola() {
    let saludo = "Hola";
}
Hola();
console.log(saludo);
````

- 5 Cuál es la salida del siguiente código?

````js
function saludar() {
    frase = "Hola";
    console.log(frase);
    var frase;
}
saludar();
````

- 6 Cuál es la salida del siguiente código?

````js
var version = '1';
var version = '2';
console.log(version);

if (true) {
  var version = '2';
  console.log(version);
}
console.log(version);
````

- 7 Cuál es la salida del siguiente código?

````js
var version = '1';
console.log(version);

function miFuncion() {
  version = '2';
  console.log(version);
}
miFuncion();
console.log(version);
````

- 8 Cuál es la salida del siguiente código?

````js
function miFuncion() {
    let x = 300;
    if (true) {
      let x = 500;
      console.log(x);
    }
    console.log(x);
  }

miFuncion();
````

- 9 Cuál es la salida del siguiente código?

let vector = [];
vector.push(1,2);
vector.push(3,4);
vector.shift(1);
vector.push(8);
console.log(vector);



- 10 Cuál es la salida del siguiente código?

````js
var a = 5;
var b = 10;

if (a === 5) {
  let a = 4; // El alcance es dentro del bloque if
  var b = 1; // El alcance es dentro de la función

  console.log(a);  // 4
  console.log(b);  // 1
}

console.log(a); // 5
console.log(b); // 1
````