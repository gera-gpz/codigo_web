// Inserción Automática de punto y coma 
// en Javascript:

// 1) Al final de una línea
// ------------------------
let a=1
let b=2
console.log('a='+a)
console.log('b='+b)



// 3) Si después de un break/continue/return/throw
//    hay un salto de línea
// -----------------------------------------------
function getMensaje() {
	var mensaje = "Hola Mundo";
	return
	mensaje;
}

console.log(getMensaje());



// -----------------------------------------------
const d = 5
const e = 10
const f = d + e

[1, 2, 3].forEach((ele) => console.log(ele))
